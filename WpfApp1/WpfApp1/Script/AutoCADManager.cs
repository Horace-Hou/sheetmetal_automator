﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Threading;

using Autodesk.AutoCAD.Interop;
using Autodesk.AutoCAD.Interop.Common;

namespace WpfApp1.Script
{
    public static class AutoCADManager
    {
        private static string acad2DSketchTemplatePathName = "\\UserDataCache\\zh-cn\\Template\\acad";
        private static string acadPath = null;
        public static string[] acadProgID = { "AutoCAD.Application.22" };

        public static string acad2DSketchTemplatePath { get { return String.Concat(acadPath, acad2DSketchTemplatePathName); } }
        private static AcadApplication acApp;

        private static Timer statTimer_connectToAcad;

        public static void Start()
        {
            if (acApp != null) return;
            StartAcad();
            //statTimer_connectToAcad = new Timer(ConnectAcad, new AutoResetEvent(false), 0, 1000);
            ConnectAcad();

        }

        public static void Exit()
        {
            acApp.Quit();
        }

        private static void StartAcad()  // need to recheck this function
        {
            if (!File.Exists(AppManager.programData.AutoCADExe_FilePath))
            {
                Console.WriteLine("AutoCAD file not exist");
                return;
            }
            if (acApp != null)
            {
                return;
            }
            try
            {
                acApp = (AcadApplication)Marshal.GetActiveObject(acadProgID[0]);
            }
            catch (Exception e)
            {
                ProcessStartInfo psi = new ProcessStartInfo(AppManager.programData.AutoCADExe_FilePath);
                psi.WorkingDirectory = Directory.GetCurrentDirectory();
                Process pr = Process.Start(psi);
            }
        }

        ///need a timer, this is for when the application is starting, however async is better
        //private static void ConnectAcad(Object stateInfo)
        //{
        //    try
        //    {
        //        int i = 0;
        //        foreach (string val in acadProgID)
        //        {
        //            i++;
        //            if (acApp == null)
        //            {
        //                try
        //                {
        //                    acApp = (AcadApplication)Marshal.GetActiveObject(val);
        //                }
        //                catch (Exception e)
        //                {
        //                    Console.WriteLine("Exception found in ConnectAcad: " + e.Data.ToString());
        //                }
        //            }
        //            else
        //            {
        //                acApp.Visible = true;
        //                statTimer_connectToAcad.Dispose();
        //                Console.WriteLine("AutoCAD application is connected");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Exception found in ConnectAcad: " + ex.Data.ToString());
        //    }
        //}

        /// <summary>
        /// Connect to autocad
        /// </summary>
        /// <param name="stateInfo"></param>
        private static void ConnectAcad()
        {
            int counter = 0;
            while(acApp== null || counter>60)
            {
                try
                {
                    int i = 0;
                    foreach (string val in acadProgID)
                    {
                        i++;
                        if (acApp == null)
                        {
                            try
                            {
                                acApp = (AcadApplication)Marshal.GetActiveObject(val);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Exception found in ConnectAcad: " + e.Data.ToString());
                            }
                        }
                        else
                        {
                            acApp.Visible = true;
                            statTimer_connectToAcad.Dispose();
                            Console.WriteLine("AutoCAD application is connected");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception found in ConnectAcad: " + ex.Data.ToString());
                }
                finally
                {
                    counter++;
                    Thread.Sleep(1000);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="paths"></param>
        /// <param name="projectName">project name without appendix</param>
        public static bool ImportDXF(string[] paths, string _projectName, string DxfSaveDiectory, AcSaveAsType type)
        {
            try
            {
                if(acApp == null) { Start(); }

                string projectName = String.Concat(_projectName, ".dwt");
                string DxfSavePath = String.Concat(DxfSaveDiectory, "\\", projectName);
                AcadDocument acadDocument = acApp.Documents.Add(projectName);
                double i = 0;
                double k = 0;
                double j = 0;
                foreach (string path in paths)
                {

                    string fileName = Path.GetFileNameWithoutExtension(path);

                    AcadDocument acadDocumentTemp = acApp.Documents.Open(path, null, null);
                    acadDocumentTemp.SendCommand(String.Concat("-BLOCK\r", fileName, "\r0\rall\r\r"));
                    acadDocumentTemp.SendCommand(String.Concat("-INSERT\r", fileName, "\r0\r1\r1\r0\r"));
                    acadDocumentTemp.SendCommand("_ai_selall\r_copyclip\r");
                    acadDocument.SendCommand(String.Concat("_pasteclip\r", (k * 3000).ToString(), ",", (j * 2500).ToString(), "\r"));
                    acadDocumentTemp.Close();
                    
                    i = i + 1d;
                    k = Math.Floor(i % 5);
                    j = Math.Floor(i / 5);
                }

                acApp.ActiveDocument.SaveAs(DxfSavePath, type);
                acadDocument.Close();

                // delete thoose single files which they are no longer required
                foreach (string path in paths)
                {
                    File.Delete(path);
                    File.Delete(String.Concat(path, ".DWG"));
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception found in importDXF: " + e.Data.ToString());
                return false;
            }
            finally
            {
                acApp.Quit();
                acApp = null;
            }
        }
        public static void SetVisibillity(bool state)
        {
            acApp.Visible = state;
        }


    }
}
