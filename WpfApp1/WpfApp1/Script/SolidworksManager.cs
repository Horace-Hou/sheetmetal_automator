﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;

using Jace;

using WpfApp1.Script;

namespace WpfApp1
{
    public static class SolidworksManager
    {
        private static SldWorks swApp;
        public static SldWorks SwApp
        {
            get
            {
                if (swApp == null)
                {
                    Console.WriteLine("swApp is null, creat a new instance of the SW application");
                    ReSetSwApp();
                }
                return swApp;
            }
            set
            {
                if (value != null) swApp = value;
            }
        }
        
        private static ModelDoc2 CurrentSwDoc;

        private static List<ModelDoc2> SwDocs = new List<ModelDoc2>();

        private static ModelView SwModelView;

        private static ModelDocExtension SwModelExt;

        private static View3D Sw3DView;

        private static FeatureManager SwFearManager;

        private static bool BoolStatus;

        private static int LogError;

        private static int LogWarning;

        

        private static SldWorks GetCurrentSolidworksSession()
        {
            return (SldWorks)Marshal.GetActiveObject("SldWorks.Application"); // do not implement
        }

        private static void ReSetSwApp()
        {
            if (swApp == null)
            {
                swApp = new SldWorks();
            }
            swApp.Visible = true;
            swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swInputDimValOnCreate, false);
            swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swExtRefNoPromptOrSave, false);
            swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swCollabTopDocsNoPromptOrSave, false);
        }

        
        #region Creating Template

        public static void Start()
        {
            try
            {
                SwApp = GetCurrentSolidworksSession();
                ReSetSwApp();
            }
            catch (Exception ex)
            {
                ReSetSwApp();
            }
        }

        public static void Exit()
        {
            if (swApp != null) swApp.ExitApp();
        }
        
        public static bool TryLoadTemplate(ref Template theTemplate, string path)
        {
            try
            {
                theTemplate.ClearAll();

                CurrentSwDoc = SwApp.OpenDoc6(path, 2, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, null, LogError, LogWarning);
                if (LogError != 0) Console.WriteLine("error:" + LogError);
                if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);

                theTemplate.AddComponent(GetComponentsData(CurrentSwDoc,Path.GetFileNameWithoutExtension(path),(swDocumentTypes_e)CurrentSwDoc.GetType()));

                AssemblyDoc swAssm = (AssemblyDoc)CurrentSwDoc;
                object[] components = (object[])swAssm.GetComponents(true);

                List<string> nameBuffs = new List<string>(); // the buff that holds all the component names

                foreach (Component2 component in components)
                {
                    ModelDoc2 modelDoc2 = (ModelDoc2)component.GetModelDoc2();

                    if (component.IsSuppressed()) continue; // exclude the suppressed component

                    // this blocks of code test if the component is a duplicated part, if it is, continue the loop
                    if (!nameBuffs.Contains(component.Name.Split('-')[0]))
                    {
                        nameBuffs.Add(component.Name.Split('-')[0]);
                    }else{
                        Console.WriteLine("This is a duplication part, the part name is " + component.Name);
                        continue;
                    }

                    // avoid the mirrored part which they will update themself
                    if (IsMirrored(modelDoc2)) continue;

                    swDocumentTypes_e docType = (swDocumentTypes_e)modelDoc2.GetType();
                    switch (docType)
                    {
                        case swDocumentTypes_e.swDocPART:                            
                            theTemplate.AddComponent(GetComponentsData(modelDoc2, component, docType));
                            break;
                        case swDocumentTypes_e.swDocASSEMBLY:
                            theTemplate.AddComponent(GetComponentsData(modelDoc2, component, docType));

                            AssemblyDoc assemblyDoc = (AssemblyDoc)modelDoc2;
                            object[] subcomponents = (object[])assemblyDoc.GetComponents(true);

                            foreach (Component2 subcomponent in subcomponents)
                            {
                                ModelDoc2 subDoc = (ModelDoc2)subcomponent.GetModelDoc2();
                                swDocumentTypes_e subDocType = (swDocumentTypes_e)subDoc.GetType();
                                theTemplate.AddComponent(GetComponentsData(subDoc, subcomponent, subDocType));
                            }
                            break;
                        default:
                            throw new System.Exception("Unspecified solidworks component file type found in the assembly.");
                    }
                }

                return true;
            }catch(Exception ex)
            {
                Console.WriteLine("Exception has been found: " + ex.Data.ToString());
                return false;
            }
        }   
        
        /// <summary>
        /// Get the data from the solidworks component, including the sheetmetal thickness, dimensions
        /// </summary>
        /// <param name="modelDoc2"></param>
        /// <param name="component"></param>
        /// <returns></returns>
        public static ComponentsData GetComponentsData(ModelDoc2 modelDoc2, Component2 component,swDocumentTypes_e docType)
        {

            ModelDocExtension modelDocExtension = modelDoc2.Extension;

            SheetMetalFeatureData sheetMetalFeatureData = TryGetSheetMetalFeatureData(modelDocExtension);
            if (sheetMetalFeatureData != null)
            {
                //Console.WriteLine("The thickness of the sheetmetal is" + sheetMetalFeatureData.Thickness);
            }

            Feature feature = modelDoc2.FirstFeature();
            List<string> dimensions = new List<string>();

            while (feature != null)
            {
                DisplayDimension displayDimension = feature.GetFirstDisplayDimension();
                while (displayDimension != null)
                {
                    Dimension dimension = displayDimension.GetDimension();
                    dimensions.Add(dimension.FullName);
                    displayDimension = feature.GetNextDisplayDimension(displayDimension);
                }
                feature = feature.GetNextFeature();
            }
            ComponentsData componentsData = new ComponentsData(component.Name, dimensions, docType);
            return componentsData;
        }

        public static ComponentsData GetComponentsData(ModelDoc2 modelDoc2, string Name, swDocumentTypes_e docType)
        {

            ModelDocExtension modelDocExtension = modelDoc2.Extension;

            SheetMetalFeatureData sheetMetalFeatureData = TryGetSheetMetalFeatureData(modelDocExtension);
            if (sheetMetalFeatureData != null)
            {
                //Console.WriteLine("The thickness of the sheetmetal is" + sheetMetalFeatureData.Thickness);
            }

            Feature feature = modelDoc2.FirstFeature();
            List<string> dimensions = new List<string>();

            while (feature != null)
            {
                DisplayDimension displayDimension = feature.GetFirstDisplayDimension();
                while (displayDimension != null)
                {
                    Dimension dimension = displayDimension.GetDimension();
                    dimensions.Add(dimension.FullName);
                    displayDimension = feature.GetNextDisplayDimension(displayDimension);
                }
                feature = feature.GetNextFeature();
            }
            ComponentsData componentsData = new ComponentsData(Name, dimensions, docType);
            return componentsData;
        }


        public static bool IsMirrored(ModelDoc2 modelDoc)
        {
            if(modelDoc.GetType()== (int)swDocumentTypes_e.swDocPART)
            {
                PartDoc partDoc = (PartDoc)modelDoc;
                if (partDoc.IsMirrored())
                {

                    Console.WriteLine("this is a mirrored part"); return true;
                }
                else return false;
            }
            return false;
        }
        

        public static SheetMetalFeatureData TryGetSheetMetalFeatureData(ModelDocExtension modelDocExtension)
        {
            Feature sheetMetalFeature = modelDocExtension.GetTemplateSheetMetal();
            SheetMetalFeatureData sheetMetalFeatureData = null;
            if (sheetMetalFeature != null)
            {
                sheetMetalFeatureData = sheetMetalFeature.GetDefinition();
                return sheetMetalFeatureData;
            }
            return null;
        }


        public static bool Visiblity
        {
            get
            {
                return SwApp.Visible;
            }
            set
            {
                swApp.Visible = (value);
            }
        }


        public static bool TrySaveNewTemplateModelFile(string templateMainAssemPath, AppTemplateData templateData)
        {
            string modelSavePath;
            string templatePath = AppManager.GetTemplate(templateData.TemplateName);
            modelSavePath = templatePath.GetModelDirectory();

            //CurrentSwDoc = SwApp.ActivateDoc3(templateMainAssemPath, false, 1, LogError);
            CurrentSwDoc = SwApp.OpenDoc6(templateMainAssemPath, 2, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, null, LogError, LogWarning);
            if (LogError != 0) Console.WriteLine("error:" + LogError);
            if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);

            if (LogError == 0)
            {
                
                SwModelExt = CurrentSwDoc.Extension;

                //Pack and go
                PackAndGo packAndGo = SwModelExt.GetPackAndGo();
                packAndGo.IncludeDrawings = true;
                packAndGo.IncludeSimulationResults = false;
                packAndGo.IncludeSuppressed = true;
                packAndGo.IncludeToolboxComponents = false;
                packAndGo.FlattenToSingleFolder = true;

                packAndGo.SetSaveToName(true, modelSavePath);

                if (!SwModelExt.SavePackAndGo(packAndGo)) { return false; }
            }

            return true;
        }

        public static bool TrySaveModelFile(string templateMainAssemPath, string ToDirectory)
        {
            CurrentSwDoc = SwApp.OpenDoc6(templateMainAssemPath, 2, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, null, LogError, LogWarning);
            //CurrentSwDoc = SwApp.ActivateDoc3(templateMainAssemPath, false, 1, LogError);
            if (LogError != 0) Console.WriteLine("error:" + LogError);
            if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);
            if (LogError == 0)
            {
                SwModelExt = CurrentSwDoc.Extension;

                //Pack and go
                PackAndGo packAndGo = SwModelExt.GetPackAndGo();
                packAndGo.IncludeDrawings = false;
                packAndGo.IncludeSimulationResults = false;
                packAndGo.IncludeSuppressed = true;
                packAndGo.IncludeToolboxComponents = false;
                packAndGo.FlattenToSingleFolder = true;
                packAndGo.SetSaveToName(true, ToDirectory);
                SwModelExt.SavePackAndGo(packAndGo);
            }
            SwApp.CloseDoc(templateMainAssemPath);
            return true;
        }

        public static bool TrySaveNewTemplateModelFileWithEdrawing(string templateMainAssemPath, AppTemplateData templateData)
        {
            try
            {
                string modelSavePath;
                string templatePath = AppManager.GetTemplate(templateData.TemplateName);
                
                modelSavePath = templatePath.GetModelDirectory();
                Console.WriteLine(modelSavePath);
                Console.WriteLine("**: " + templateMainAssemPath);
                //CurrentSwDoc = SwApp.OpenDoc6(templateMainAssemPath, 2, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, null, LogError, LogWarning);
                CurrentSwDoc = SwApp.ActivateDoc3(templateMainAssemPath,false,1, LogError);

                if (LogError != 0) Console.WriteLine("error:" + LogError);
                if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);

                if (LogError == 0)
                {

                    if (CurrentSwDoc.Extension == null) { Console.WriteLine("model doc extension is null"); }
                    SwModelExt = CurrentSwDoc.Extension;

                    //Pack and go
                    PackAndGo packAndGo = SwModelExt.GetPackAndGo();
                    packAndGo.IncludeDrawings = true;
                    packAndGo.IncludeSimulationResults = false;
                    packAndGo.IncludeSuppressed = true;
                    packAndGo.IncludeToolboxComponents = false;
                    packAndGo.FlattenToSingleFolder = true;
                    packAndGo.SetSaveToName(true, modelSavePath);
                    SwModelExt.SavePackAndGo(packAndGo);

                    string edrawingName = String.Concat(templatePath.GetEdrawingDirectory(),"\\",templateData.TemplateName ,".easm");
                    int options = (int)swSaveAsOptions_e.swSaveAsOptions_Silent + (int)swSaveAsOptions_e.swSaveAsOptions_AvoidRebuildOnSave;

                    Console.WriteLine(edrawingName);
                    SwModelExt = CurrentSwDoc.Extension;
                    SwModelExt.SaveAs(edrawingName, (int)swSaveAsVersion_e.swSaveAsCurrentVersion, options, null, LogError, LogWarning);
                    if (LogError != 0) Console.WriteLine("error:" + LogError);
                    if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);
                }
            }
            catch (Exception ex)
            {
                return false;
            }       

            

            return true;
        }


        #endregion


        #region Creating project

        /// <summary>
        /// remember to pack and go the file into a temporary folder first
        /// </summary>
        public static bool TryCreatANewSolidworksModel(ref Template theTemplate)
        {
            CalculationEngine engine = new CalculationEngine();
            Dictionary<string, double> Inputs = new Dictionary<string, double>();
            Dictionary<string, double> Dimensions = new Dictionary<string, double>();

            foreach(InputData input in theTemplate.ProjectInputs) // becareful with the input and projectInput, first one is for template and second one is for the project
            {
                Inputs.Add(input.Name,input.DefaultValue);
            }
            foreach(DimensionData dimension in theTemplate.Dimensions)
            {
                Dimensions.Add(dimension.SelectedDimension, engine.Calculate(dimension.Formula, Inputs).ToMeter());
            }

            try
            {
                // close the current working session if there is one
                SwApp.CloseAllDocuments(true);
                AppManager.ClearTemporaryProjectDirectory();
                string FilePath;
                string templateFilePath = theTemplate.Name.GetModelMainAssem();

                if (templateFilePath == null) return false;
                if (TrySaveModelFile(templateFilePath, AppManager.TemporaryProjectDirectory))
                {
                    FilePath = AppManager.TemporaryProjectDirectory.GetModelMainAssemInThatDirectory();

                    if (FilePath == null) Console.WriteLine("Filepath is null");

                    Console.WriteLine("TheFilePath is" + FilePath);
                    CurrentSwDoc = SwApp.OpenDoc6(FilePath, 2, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, null, LogError, LogWarning);
                    if (LogError != 0) Console.WriteLine("error:" + LogError);
                    if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);
                    foreach (var dimension in Dimensions)
                    {
                        Dimension sldDimension = (Dimension)CurrentSwDoc.Parameter(dimension.Key);
                        sldDimension.SystemValue = dimension.Value;
                    }
                    CurrentSwDoc.ForceRebuild3(true);
                    if (CurrentSwDoc.Save3((int)swSaveAsOptions_e.swSaveAsOptions_OverrideSaveEmodel, LogError, LogWarning))
                    {
                        Console.WriteLine("Save successfully");
                        //swApp.CloseAllDocuments(false);
                    }
                    else
                    {
                        if (LogError != 0) Console.WriteLine("error:" + LogError);
                        if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception found: " + ex.Data.ToString());
                return false;
            }          
            return true;
        }


        public static bool TryCreatingDXFFilesFromTemporaryProject(MarkPointEnum markPointEnum)
        {
            try
            {
                AppManager.ClearTemporary2DCutingFileFolder();
                string FilePath = AppManager.TemporaryProjectAssemDirectory;
                string temporaryCutingDrawingsDirectory = AppManager.TemporaryCutingDrawingsDirectory;
                string temporaryCutingDrawingDirectory = AppManager.TemporaryCutingDrawingDirectory;

                CurrentSwDoc = SwApp.OpenDoc6(FilePath, 2, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, null, LogError, LogWarning);
                if (LogError != 0) { Console.WriteLine("error:" + LogError); return false; };
                if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);


                if (CurrentSwDoc == null) { Console.WriteLine("Current solidworks model instance is null"); return false; };


                AssemblyDoc swAssm = (AssemblyDoc)CurrentSwDoc;
                object[] components = (object[])swAssm.GetComponents(false);

                foreach (Component2 component in components)
                {
                    swAssm.ForceRebuild2(true);
                    ModelDoc2 modelDoc2 = (ModelDoc2)component.GetModelDoc2();
                    //modelDoc2.ForceRebuild3(true); //must rebuild the assembly after export the flatpattern view since the reference might be changed due to flatten operation for mark points

                    swDocumentTypes_e docType = (swDocumentTypes_e)modelDoc2.GetType();

                    switch (docType)
                    {
                        case swDocumentTypes_e.swDocASSEMBLY:
                            Console.WriteLine("This is a assembly: " + component.Name2);
                            break;
                        case swDocumentTypes_e.swDocPART:
                            if (!IsSheetMetal((PartDoc)modelDoc2)) { Console.WriteLine("This is not a sheetmetal"); break; }

                            string sPathName = String.Concat(temporaryCutingDrawingsDirectory + "\\" + component.Name2 + ".DXF"); // destination
                            string compName = component.GetPathName(); //the original model file

                            // Handle the mirrored components
                            if (component.IsMirrored())
                            {
                                Feature feature = modelDoc2.FirstFeature();
                                while (feature != null)
                                {
                                    if (feature.GetTypeName2() == "MirrorStock")
                                    {
                                        MirrorPartFeatureData mirrorPartFeatureData = feature.GetDefinition();
                                        compName = mirrorPartFeatureData.PathName;
                                        break;
                                    }
                                    feature = feature.GetNextFeature();
                                }
                            }
                            if (!TryExportDXF(sPathName, compName, markPointEnum))
                            {
                                return false;
                            }

                            break;

                    }

                }
                swApp.CloseDoc(FilePath);
                //swApp.ExitApp();
                return true;
            }catch(Exception e)
            {
                throw new System.Exception("Exception found in TryCreatingDXFFilesFromTemporaryProject: " + e.Data.ToString());
            }
            
        }        

        /// <summary>
        /// Suppress the and unsupress the wanted markpoint
        /// </summary>
        /// <param name="markPointType"></param>
        /// <param name="modelDoc2"></param>
        /// <returns></returns>
        private static List<Feature> SuppressAndUnSuppressWantedMarkPointType(MarkPointEnum markPointType, ModelDoc2 modelDoc2)
        {
            string name = "";
            string nameOppose1 = "";
            List<Feature> reSuppressedFeatures = new List<Feature>();

            Console.WriteLine("Marker point type: " + markPointType.ToString());

            switch (markPointType)
            {
                case MarkPointEnum.PlasmaMarkPoints:
                    name = "PlasmaMarkPoints";
                    //nameOppose1 = "RoterMarkPoints";
                    break;
                case MarkPointEnum.RoterMarkPoints:
                    name = "RoterMarkPoints";
                    //nameOppose1 = "PlasmaMarkPoints";
                    break;
                default:
                    break;

            }
            FeatureManager featureManager = modelDoc2.FeatureManager;
            object[] featuresObj = featureManager.GetFeatures(true);
            foreach (object featureObj in featuresObj)
            {
                Feature feature = (Feature)featureObj;
                if (feature.GetTypeName2() == "ICE")
                {
                    if (feature.Name == name)
                    {
                        feature.SetSuppression2((int)swFeatureSuppressionAction_e.swUnSuppressFeature, (int)swInConfigurationOpts_e.swThisConfiguration, null);
                        reSuppressedFeatures.Add(feature);
                    }
                    //if (feature.Name == nameOppose1)
                    //{
                    //    feature.SetSuppression2((int)swFeatureSuppressionAction_e.swSuppressFeature, (int)swInConfigurationOpts_e.swAllConfiguration, null);
                    //}                    
                }
                if(feature.GetTypeName2() == "FlatPattern")
                {
                    Console.WriteLine("Flat pattern found");
                    reSuppressedFeatures.Add(feature);
                }
            }
            return reSuppressedFeatures;
        }


        private static void SetFeaturesSuppress(List<Feature> features, swFeatureSuppressionAction_e swFeatureSuppressionAction_E)
        {
            foreach (Feature feature in features)
            {
                feature.SetSuppression2((int)swFeatureSuppressionAction_E, (int)swInConfigurationOpts_e.swThisConfiguration, null);
            }
        }


        private static bool TryExportDXF(string _sPathName, string modelPathName,MarkPointEnum markPointEnum)
        {
            bool retVal = false;
            Console.WriteLine("sPathName is: " + _sPathName);
            //Console.WriteLine("modelPathName is: " + modelPathName);

            string sPathName = _sPathName.Replace("/","_");

            ModelDoc2 swModel = (ModelDoc2)swApp.ActivateDoc3(modelPathName, false, (int)swRebuildOnActivation_e.swDontRebuildActiveDoc, ref LogError);
            if (LogError != 0) Console.WriteLine("error:" + LogError);

            // if the document has been openned and closed, it will change the default file path, which requires to reopen it
            if(swModel == null)
            {
                string modelName = Path.GetFileNameWithoutExtension(modelPathName);
                swModel = (ModelDoc2)swApp.ActivateDoc3(modelName, false, (int)swRebuildOnActivation_e.swDontRebuildActiveDoc, ref LogError);

                //Console.WriteLine("the mirrored file without file extension is: " + modelName);
                modelPathName = swModel.GetPathName();

                if (swModel == null)
                {
                    Console.WriteLine("model doc is null");
                    return false;
                }
            }

            //change the mark point type upon selection
            List<Feature> rerSuppressedfeatures = SuppressAndUnSuppressWantedMarkPointType(markPointEnum, swModel);

            PartDoc partDoc = (PartDoc)swModel;

            double[] dataAlignment = new double[12];
            dataAlignment[0] = 0.0;
            dataAlignment[1] = 0.0;
            dataAlignment[2] = 0.0;
            dataAlignment[3] = 0.0;
            dataAlignment[4] = 0.0;
            dataAlignment[5] = 0.0;
            dataAlignment[6] = 0.0;
            dataAlignment[7] = 0.0;
            dataAlignment[8] = 0.0;
            dataAlignment[9] = 0.0;
            dataAlignment[10] = 0.0;
            dataAlignment[11] = 0.0;
            
            int options = 1;  //include flat-pattern geometry

            // select all the solid bodies
            object[] objects = (object[])partDoc.GetBodies2((int)swBodyType_e.swSolidBody, true);

            foreach (object obj in objects)
            {
                Body2 body = (Body2)obj;
                Console.WriteLine("The body name is:  " + body.Name);
                swModel.Extension.SelectByID2(body.Name, "", 0, 0, 0, true, 0, null, (int)swSelectOption_e.swSelectOptionDefault);
            }
            retVal = partDoc.ExportToDWG2(sPathName, modelPathName, (int)swExportToDWG_e.swExportToDWG_ExportSheetMetal, true, (object)dataAlignment, false, false, options, null);

            //fold back the flatpattern to avoid the update in the assembly that might cause error
            SetFeaturesSuppress(rerSuppressedfeatures, swFeatureSuppressionAction_e.swSuppressFeature);

            //retVal = partDoc.ExportFlatPatternView(sPathName, (int)swExportFlatPatternViewOptions_e.swExportFlatPatternOption_RemoveBends);
            swApp.QuitDoc(modelPathName);

            return retVal;
        }

        private static bool IsSheetMetal(PartDoc partDoc)
        {
            // select all the bodies
            try
            {
                object[] objects = (object[])partDoc.GetBodies2((int)swBodyType_e.swSolidBody, true);
                if (objects == null)
                {
                    return false;
                }
                foreach (object obj in objects)
                {
                    Body2 body = (Body2)obj;
                    if (!body.IsSheetMetal())
                    {
                        Console.WriteLine();
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool TryOpenCurrentSldworkTemplate(string SolidworksFileName)
        {
            try {
                CurrentSwDoc = SwApp.OpenDoc6(SolidworksFileName, 2, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, null, LogError, LogWarning);
                if (LogError != 0) { Console.WriteLine("error:" + LogError); return false; };
                if (LogWarning != 0) Console.WriteLine("warning:" + LogWarning);
                return true;
            }catch(Exception ex){
                throw new System.Exception("Exception found in TryOpenCurrentSldworkTemplate.: "+ ex.Data.ToString());                
            }

        }
        #endregion



        #region Merge 2D cut plot
        public static bool MergeDXF(string TargetPath, string FileName)
        {
            return AutoCADManager.ImportDXF(Directory.GetFiles(AppManager.TemporaryCutingDrawingsDirectory), FileName,TargetPath,Autodesk.AutoCAD.Interop.Common.AcSaveAsType.ac2000_dxf);
        }

        #endregion



    }


}
