﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
//using System.Windows.Controls;
//using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows;

using WpfApp1.Script;

using SolidWorks.Interop.swconst;
using SolidWorks.Interop.sldworks;
using eDrawingHostControl;


/// <summary>
/// This script contains all the properties for the model view view model
/// </summary>

namespace WpfApp1
{

    /// <summary>
    /// 
    /// </summary>\
    public class Template : INotifyPropertyChanged
    {
        public Template(string name, string filePath)
        {
            Name = name;
            FilePath = filePath;
        }

        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        private string name;
        public string Name { get { return name; } set { name = value;
                Console.WriteLine(name);
                PropertyChanged(this, new PropertyChangedEventArgs("Name")); } }

        private string filePath;
        public string FilePath { get { return filePath; } set { filePath = value;
                Console.WriteLine("File path has changed," + filePath);
                PropertyChanged(this, new PropertyChangedEventArgs("FilePath")); } }

        private TemplateState state;
        public TemplateState State
        {
            get { return state; }
            set
            {
                SetState(value);
                state = value;                                           
            }
        }

        private string descriptionBox;
        public string DescriptionBox
        {
            get { return descriptionBox; }
            set
            {
                descriptionBox = value;
                PropertyChanged(this, new PropertyChangedEventArgs("DescriptionBox"));
            }
        }

        private string selectedTemplateName;
        public string SelectedTemplateName
        {
            get { return selectedTemplateName; }
            set
            {
                selectedTemplateName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SelectedTemplateName"));
            }
        }

        private int markPointOptionComboBox;
        public int MarkPointOptionComboBox
        {
            get { return markPointOptionComboBox; }
            set
            {
                markPointOptionComboBox = value;
                PropertyChanged(this, new PropertyChangedEventArgs("MarkPointOptionComboBox"));
            }
        }

        private int previewEdrawingItemIndex;
        public int PreviewEdrawingItemIndex
        {
            get
            {
                return previewEdrawingItemIndex;
            }
            set
            {
                previewEdrawingItemIndex = value;
                if (value >= PreviewEdrawingItems.Count)
                {
                    previewEdrawingItemIndex = (PreviewEdrawingItems.Count - 1);                   
                }
                if (value < 0)
                {
                    previewEdrawingItemIndex = 0;                    
                }
                if (PreviewEdrawingItems.Count == 1)
                {
                    EdrawingItemsForwardButtonEnabled = false;
                    EdrawingItemsBackButtonEnabled = false;
                    EdrawingItemsForwardButtonOpacity = 0.5d;
                    EdrawingItemsBackButtonOpacity = 0.5d;
                    return;
                }
                if(previewEdrawingItemIndex <= 0)
                {
                    EdrawingItemsForwardButtonEnabled = true;                    
                    EdrawingItemsBackButtonEnabled = false;
                    EdrawingItemsForwardButtonOpacity = 1d;
                    EdrawingItemsBackButtonOpacity = 0.5d;                    
                }
                if (previewEdrawingItemIndex >= (PreviewEdrawingItems.Count - 1))
                {
                    EdrawingItemsForwardButtonEnabled = false;
                    EdrawingItemsBackButtonEnabled = true;
                    EdrawingItemsForwardButtonOpacity = 0.5d;
                    EdrawingItemsBackButtonOpacity = 1d;                    
                }

                SetUpNewEdrawingPath(PreviewEdrawingItems[previewEdrawingItemIndex]);
                //PropertyChanged(this, new PropertyChangedEventArgs("PreviewEdrawingItemIndex"));
            }
        }        

        public List<string> PreviewEdrawingItems = new List<string>();

        private bool edrawingItemsForwardButtonEnabled;
        public bool EdrawingItemsForwardButtonEnabled
        {
            get { return edrawingItemsForwardButtonEnabled; }
            set
            {
                edrawingItemsForwardButtonEnabled = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EdrawingItemsForwardButtonEnabled"));
            }
        }

        private bool edrawingItemsBackButtonEnabled;
        public bool EdrawingItemsBackButtonEnabled
        {
            get { return edrawingItemsBackButtonEnabled; }
            set
            {
                edrawingItemsBackButtonEnabled = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EdrawingItemsBackButtonEnabled"));
            }
        }

        private double edrawingItemsForwardButtonOpacity;
        public double EdrawingItemsForwardButtonOpacity
        {
            get { return edrawingItemsForwardButtonOpacity; }
            set
            {
                edrawingItemsForwardButtonOpacity = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EdrawingItemsForwardButtonOpacity"));
            }
        }

        private double edrawingItemsBackButtonOpacity;
        public double EdrawingItemsBackButtonOpacity
        {
            get { return edrawingItemsBackButtonOpacity; }
            set
            {
                edrawingItemsBackButtonOpacity = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EdrawingItemsBackButtonOpacity"));
            }
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
            set
            {
                userName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("UserName"));
            }
        }

        private bool passwordShow;
        public bool PasswordShow
        {
            get{ return passwordShow; }
            set
            {
                passwordShow = value;
                PropertyChanged(this, new PropertyChangedEventArgs("PasswordShow"));
            }
        }

        private bool loginState;
        public bool LoginState
        {
            get { return loginState; }
            set
            {
                loginState = value;
                PropertyChanged(this, new PropertyChangedEventArgs("LoginState"));
            }
        }

        private bool templateNameState;
        public bool TemplateNameState{ get{ return templateNameState; } set{ templateNameState = value; PropertyChanged(this, new PropertyChangedEventArgs("TemplateNameState"));}}

        private bool templateAssemPathState;
        public bool TemplateAssemPathState { get { return templateAssemPathState; } set { templateAssemPathState = value; PropertyChanged(this, new PropertyChangedEventArgs("TemplateAssemPathState")); } }

        private bool templateAssemButtonState;
        public bool TemplateAssemButtonState { get { return templateAssemButtonState; } set { templateAssemButtonState = value; PropertyChanged(this, new PropertyChangedEventArgs("TemplateAssemButtonState")); } }

        private bool mainWindowState = true;
        public bool MainWindowState { get { return mainWindowState; } set { mainWindowState = value; PropertyChanged(this, new PropertyChangedEventArgs("MainWindowState")); } }

        private ObservableCollection<ComponentsData> components = new ObservableCollection<ComponentsData>();
        public ObservableCollection<ComponentsData> Components
        {
            get { return components; }
            set
            {
                components = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Components"));
            }
        }

        private ObservableCollection<DimensionData> dimensions = new ObservableCollection<DimensionData>();
        public ObservableCollection<DimensionData> Dimensions
        {
            get { return dimensions; }
            set
            {
                dimensions = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Dimensions"));
            }
        }

        private ObservableCollection<InputData> inputs = new ObservableCollection<InputData>();
        public ObservableCollection<InputData> Inputs
        {
            get { return inputs; }
            set
            {
                inputs = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Input"));
            }
        }

        private ObservableCollection<string> templateNames = new ObservableCollection<string>();
        public ObservableCollection<string> TemplateNames
        {
            get { return templateNames; }
            set
            {
                templateNames = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TemplateNames"));
            }
        }        

        private ObservableCollection<InputData> projectInputs = new ObservableCollection<InputData>();
        public ObservableCollection<InputData> ProjectInputs
        {
            get { return projectInputs; }
            set
            {
                projectInputs = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ProjectInputs"));
            }
        }

        private Visibility previewPanelVisibility;
        public Visibility PreviewPanelVisibility
        {
            get { return previewPanelVisibility; }
            set
            {
                previewPanelVisibility = value;
                PropertyChanged(this, new PropertyChangedEventArgs("PreviewPanelVisibility"));
            }
        }

        private Visibility projectPanelVisibility;
        public Visibility ProjectPanelVisibility
        {
            get { return projectPanelVisibility; }
            set
            {
                projectPanelVisibility = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ProjectPanelVisibility"));
            }
        }

        private Visibility sLDEModelVisibility;
        public Visibility SLDEModelVisibility
        {
            get { return sLDEModelVisibility; }
            set
            {
                sLDEModelVisibility = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SLDEModelVisibility"));
            }
        }

        private bool flyout1IsOpen;
        public bool Flyout1IsOpen
        {
            get { return flyout1IsOpen; }
            set
            {
                flyout1IsOpen = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Flyout1IsOpen"));
            }
        }

        private string flyout1Heading;
        public string Flyout1Heading
        {
            get { return flyout1Heading; }
            set
            {
                flyout1Heading = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Flyout1Heading"));
            }
        }

        private BitmapImage userIcon;
        public BitmapImage UserIcon
        {
            get { return userIcon; }
            set
            {
                userIcon = value;
                PropertyChanged(this, new PropertyChangedEventArgs("UserIcon"));
            }
        }

        private GridLength columnDefinition_1;
        public GridLength ColumnDefinition_1
        {
            get
            {
                return columnDefinition_1;
            }
            set
            {
                columnDefinition_1 = value;
                PropertyChanged(this,new PropertyChangedEventArgs("ColumnDefinition_1"));
            }
        }

        private GridLength columnDefinition_2;
        public GridLength ColumnDefinition_2
        {
            get
            {
                return columnDefinition_2;
            }
            set
            {
                columnDefinition_2 = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ColumnDefinition_2"));
            }
        }

        private eDrawingControl templateEdrawingControl1 = new eDrawingControl();
        public eDrawingControl TemplateEdrawingControl1 { get { return templateEdrawingControl1; } set {; } }
        private eDrawingControl templateEdrawingControl2 = new eDrawingControl();
        public eDrawingControl TemplateEdrawingControl2 { get { return templateEdrawingControl2; } set {; } }        

        public static readonly BitmapImage Solidworks_Icon = new BitmapImage(new Uri("pack://application:,,,/Images/Solidworks_Icon.png"));
        public static readonly BitmapImage Solidworks_PartIcon = new BitmapImage(new Uri("pack://application:,,,/Images/Solidworks_PartIcon.png"));

        public ComponentsData selectedComponent = null;

        private double progressValue;
        public double ProgressValue
        {
            get { return progressValue; }
            set
            {
                progressValue = value;
                PropertyChanged(this,new PropertyChangedEventArgs("ProgressValue"));
            }
        }

        public void ReSetProperties(string _name, string _filePath, List<TemplateComponentData> _componentsData,
            List<TemplateDimensionData> _dimensionsData, List<TemplateInputData> _inputData, bool _isPropertiesConfirmed)
        {
            Name = _name;
            FilePath = _filePath;

            // clear the data of the class
            ClearAll();

            foreach (TemplateComponentData data in _componentsData)
            {
                AddComponent(new ComponentsData(data.ComponentName,data.Dimensions,data.Type));
            }
            foreach (TemplateDimensionData data in _dimensionsData)
            {
                AddADimension(new DimensionData(data.Dimensions,data.DimensionFormula,data.DimensionName, _isPropertiesConfirmed));
            }
            foreach (TemplateInputData data in _inputData)
            {
                Inputs.Add(new InputData(data.InputName,data.InputDefaultValue, _isPropertiesConfirmed, data.MinimumValue, data.MaximumValue));
            }
        }

        public void AddSolidworksPart(string _context, List<string> dimension)
        {
            Components.Add(new ComponentsData(_context, dimension, swDocumentTypes_e.swDocPART));
            Console.WriteLine("Soldworks part add, there is currently " + Components.Count + " components in the document.");
        }

        public void AddComponent(ComponentsData componentsData)
        {
            Console.WriteLine("here" + componentsData.sldDocumentType.ToString());
            if (componentsData.sldDocumentType == swDocumentTypes_e.swDocPART)
            {
                AddSolidworksPart(componentsData.Name, componentsData.Dimension);
            }
            else if (componentsData.sldDocumentType == swDocumentTypes_e.swDocASSEMBLY)
            {
                AddSolidworksAssembly(componentsData.Name, componentsData.Dimension);
            }
        }

        public void AddSolidworksAssembly(string _context, List<string> dimension)
        {
            Components.Add(new ComponentsData(_context, dimension, swDocumentTypes_e.swDocASSEMBLY));
            Console.WriteLine("Solidworks assembly add");
        }

        public void AddADimension(List<string> Names, string Formula)
        {
            dimensions.Add(new DimensionData(Names, Formula));
        }

        public void AddADimension(DimensionData data)
        {
            dimensions.Add(data);
        }

        public void AddAnInput(string Name, double defaultValue)
        {
            Inputs.Add(new InputData(Name, defaultValue));
        }

        public void SetUpNewEdrawingPath(string path)
        {
            templateEdrawingControl1.eDrawingControlWrapper.CloseActiveDoc(templateEdrawingControl1.eDrawingControlWrapper.FileName);                     
            templateEdrawingControl2.eDrawingControlWrapper.CloseActiveDoc(templateEdrawingControl1.eDrawingControlWrapper.FileName);
            //templateEdrawingControl2.eDrawingControlWrapper.ClearSelections();
            //templateEdrawingControl1.eDrawingControlWrapper.ClearSelections();
            templateEdrawingControl1.eDrawingControlWrapper.OpenDoc(path, false, false, false, "");
            templateEdrawingControl2.eDrawingControlWrapper.OpenDoc(path, false, false, false, "");
        }

        public void CloseTemplateEdrawing()
        {
            templateEdrawingControl1.eDrawingControlWrapper.CloseActiveDoc(templateEdrawingControl1.eDrawingControlWrapper.FileName);
            templateEdrawingControl2.eDrawingControlWrapper.CloseActiveDoc(templateEdrawingControl1.eDrawingControlWrapper.FileName);
        }

        public void RemoveLastDimension()
        {
            if (Dimensions.Count == 0) return;
            Dimensions.RemoveAt(Dimensions.Count - 1);
        }

        public void RemoveLastInput()
        {
            if (Inputs.Count == 0) return;
            Inputs.RemoveAt(Inputs.Count - 1);
        }

        public void SetTemplateNames(string[] names)
        {
            TemplateNames.Clear();
            foreach (string name in names)
            {
                TemplateNames.Add(name);
            }
        }

        private void SetState(TemplateState _state)
        {
            switch (_state)
            {
                case TemplateState.NewTemplate:
                    TemplateNameState = true;
                    TemplateAssemPathState = true;
                    TemplateAssemButtonState = true;
                    break;
                case TemplateState.EditTemplate:
                    TemplateNameState = false;
                    TemplateAssemPathState = false;
                    TemplateAssemButtonState = false;
                    break;
                case TemplateState.TemplatePreview:
                    //SelectedTemplateName = "";
                    ColumnDefinition_1 = new GridLength(0, GridUnitType.Star);
                    ColumnDefinition_2 = new GridLength(1, GridUnitType.Star);
                    PreviewPanelVisibility = Visibility.Visible;
                    ProjectPanelVisibility = Visibility.Collapsed;
                    ProjectInputs.Clear();
                    break;
                case TemplateState.CreateANewProject:
                    MarkPointOptionComboBox = 1;
                    ColumnDefinition_1 = new GridLength(1,GridUnitType.Star);
                    ColumnDefinition_2 = new GridLength(0, GridUnitType.Star);
                    PreviewPanelVisibility = Visibility.Collapsed;
                    ProjectPanelVisibility = Visibility.Visible;                    
                    break;
                case TemplateState.CreatingProject:
                    
                    break;
                default:
                    break;
            }
        }

        public void ClearAll()
        {
            Components.Clear();
            Dimensions.Clear();
            Inputs.Clear();
            //SelectedTemplateName = "";
            CloseTemplateEdrawing();
            //filePath = null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ComponentsData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public ComponentsData(string name, List<string> dimension, swDocumentTypes_e docType)
        {
            if (docType == swDocumentTypes_e.swDocPART)
            {
                Icon = Template.Solidworks_PartIcon;
            }
            else if (docType == swDocumentTypes_e.swDocASSEMBLY)
            {
                Icon = Template.Solidworks_Icon;
            }
            else
            {
                throw new System.Exception("Currently this sldworks file type is not excepted");
            }
            Name = name;
            Dimension = dimension;
            sldDocumentType = docType;
        }
        private List<string> dimension = new List<string>();
        public List<string> Dimension { get { return dimension; } set { dimension = value; } }

        //public Dictionary<string, string> dimensionDataList = new Dictionary<string, string>();

        public BitmapImage Icon { get; set; }
        private string name;
        public string Name { get { return name; } set { name = value; PropertyChanged(this, new PropertyChangedEventArgs("Name")); } }

        public swDocumentTypes_e sldDocumentType { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class DimensionData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public DimensionData(List<string> _names, string _formula)
        {
            Names = _names;
            Formula = _formula;
        }
        public DimensionData(List<string> _names, string _formula, string _selectedDimension)
        {
            Names = _names;
            Formula = _formula;
            SelectedDimension = _selectedDimension;
        }

        public DimensionData(List<string> _names, string _formula, string _selectedDimension, bool _isConfirmed)
        {
            Names = _names;
            Formula = _formula;
            SelectedDimension = _selectedDimension;
            IsConfirmed = _isConfirmed;
        }

        public DimensionData(string _formula, string _selectedDimension)
        {
            Formula = _formula;
            SelectedDimension = _selectedDimension;
        }

        private string selectedDimension;
        public string SelectedDimension { get { return selectedDimension; } set { selectedDimension = value; PropertyChanged(this, new PropertyChangedEventArgs("SelectedDimension")); } }

        private List<string> names = new List<string>();
        public List<string> Names { get { return names; } set { names = value; PropertyChanged(this, new PropertyChangedEventArgs("Names")); } }

        private string formula;
        public string Formula { get { return formula; } set { formula = value;
                Console.WriteLine(value);
                PropertyChanged(this, new PropertyChangedEventArgs("Formula")); } }

        private bool isNotConfirmed = true;
        public bool IsNotConfirmed {
            get { return isNotConfirmed; }
            set {
                isNotConfirmed = value;
                if (IsConfirmed == IsNotConfirmed) { IsConfirmed = !value; }
                PropertyChanged(this, new PropertyChangedEventArgs("IsNotConfirmed")); }
        }
        private bool isConfirmed = false;
        public bool IsConfirmed {
            get { return isConfirmed; }
            set {
                isConfirmed = value;
                if (IsNotConfirmed == IsConfirmed) { IsNotConfirmed = !value; }
                PropertyChanged(this, new PropertyChangedEventArgs("IsConfirmed"));
            }
        }
    }    

    /// <summary>
    /// 
    /// </summary>
    public class InputData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public InputData(string name, double defaultValue)
        {
            Name = name;
            DefaultValue = defaultValue;
        }
        public InputData(string _name, double _defaultValue, bool _isConfirmed, double _minimumValue, double _maximumValue)
        {
            Name = _name;
            DefaultValue = _defaultValue;
            IsConfirmed = _isConfirmed;
            MinimumValue = _minimumValue;
            MaximumValue = _maximumValue;
        }

        private string name;
        public string Name { get { return name; } set { name = value; PropertyChanged(this, new PropertyChangedEventArgs("Name")); } }

        private double defaultValue;
        public double DefaultValue
        {
            get
            {
                return defaultValue;
            }
            set
            {
                defaultValue = value;
                PropertyChanged(this, new PropertyChangedEventArgs("DefaultValue"));
            }
        }

        private double minimumValue;
        public double MinimumValue
        {
            get
            {
                return minimumValue;
            }
            set
            {
                minimumValue = value;
                PropertyChanged(this, new PropertyChangedEventArgs("MinimumValue"));
            }
        }

        private double maximumValue;
        public double MaximumValue
        {
            get
            {
                return maximumValue;
            }
            set
            {
                maximumValue = value;
                PropertyChanged(this, new PropertyChangedEventArgs("MaximumValue"));
            }
        }

        private bool isNotConfirmed = true;
        public bool IsNotConfirmed
        {
            get { return isNotConfirmed; }
            set
            {
                isNotConfirmed = value;
                if (IsConfirmed == IsNotConfirmed) { IsConfirmed = !value; }
                PropertyChanged(this, new PropertyChangedEventArgs("IsNotConfirmed"));
            }
        }
        private bool isConfirmed = false;
        public bool IsConfirmed
        {
            get { return isConfirmed; }
            set
            {
                isConfirmed = value;
                if (IsNotConfirmed == IsConfirmed) { IsNotConfirmed = !value; }
                PropertyChanged(this, new PropertyChangedEventArgs("IsConfirmed"));
            }
        }
    }
    

    public enum TemplateState
    {
        NewTemplate,
        EditTemplate,
        CreateANewProject,
        TemplatePreview,
        CreatingProject
    }

    public enum MarkPointEnum
    {
        RoterMarkPoints,
        PlasmaMarkPoints        
    }
}


  