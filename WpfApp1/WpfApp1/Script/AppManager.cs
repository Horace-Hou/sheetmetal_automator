﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using WpfApp1.Script;
using System.Runtime.Serialization.Formatters.Binary;

namespace WpfApp1
{
    public static class AppManager
    {
        public static ProgramData programData = new ProgramData();

        private const string programFilesDirectory = "\\Data\\ProgramFiles\\ProgramData.dat";
        public static string ProgramFilesDirectory = String.Concat(Directory.GetCurrentDirectory(), programFilesDirectory);

        private const string templateDirectory = "\\Data\\DataFiles\\Template";
        public static string TemplateDirectory = String.Concat(Directory.GetCurrentDirectory(), templateDirectory);

        private const string projectDirectory = "\\Data\\DataFiles\\Project";
        public static string ProjectDirectory = String.Concat(Directory.GetCurrentDirectory(), projectDirectory);

        private const string temporaryProjectDirectory = "\\Data\\DataFiles\\Project\\Temporary";
        public static string TemporaryProjectDirectory = String.Concat(Directory.GetCurrentDirectory(), temporaryProjectDirectory);

        private const string temporaryProjectAssemDirectory_1 = "\\Data\\DataFiles\\Project\\Temporary\\assem1.sldasm";
        private const string temporaryProjectAssemDirectory_2 = "\\Data\\DataFiles\\Project\\Temporary\\assem2.sldasm";
        private const string temporaryProjectAssemDirectory_3 = "\\Data\\DataFiles\\Project\\Temporary\\Assem2.sldasm";
        private const string temporaryProjectAssemDirectory = "\\Data\\DataFiles\\Project\\Temporary\\Assem1.sldasm";
        public static string TemporaryProjectAssemDirectory {
            get
            {
                string str = String.Concat(Directory.GetCurrentDirectory(), temporaryProjectAssemDirectory);

                if (File.Exists(str)) { return str; }
                else { str = String.Concat(Directory.GetCurrentDirectory(), temporaryProjectAssemDirectory_1); }

                if (File.Exists(str)) { return str; }
                else { str = String.Concat(Directory.GetCurrentDirectory(), temporaryProjectAssemDirectory_2); }

                if (File.Exists(str)) { return str; }
                else { str = String.Concat(Directory.GetCurrentDirectory(), temporaryProjectAssemDirectory_3); }

                if (File.Exists(str)) { return str; }
                else { throw new System.Exception("File do not exist"); }
            }
        }

        private const string temporaryCutingDrawingsDirectory = "\\Data\\DataFiles\\Project\\TemporaryCutingDrawings";
        public static string TemporaryCutingDrawingsDirectory {
            get
            {
                string str = String.Concat(Directory.GetCurrentDirectory(), temporaryCutingDrawingsDirectory);
                if (!Directory.Exists(str))
                {
                    Directory.CreateDirectory(str);
                }
                return str;
            }
        }

        private const string temporaryCutingDrawingDirectory = "\\Data\\DataFiles\\Project\\TemporaryCutingDrawing";
        public static string TemporaryCutingDrawingDirectory
        {            
            get
            {
                string str = String.Concat(Directory.GetCurrentDirectory(), temporaryCutingDrawingDirectory);
                if (!Directory.Exists(str))
                {
                    Directory.CreateDirectory(str);
                }
                return str;
            }
        }

        public const string eDrawingDirectory = "\\edrawing";
        public const string modelDirectory = "\\model";
        public const string cutDrawing = "\\drawingcut";
        public const string drawing2d = "\\drawing2d";

        public static Dictionary<string,string> accountInfo = new Dictionary<string,string>();


        #region Program region 

        public static void OnStart()
        {
            ReadProgramFilesDirectory();
            accountInfo.Add("MWqueensland", "Robert");
            accountInfo.Add("MWsydney", "Horace");
        }

        public static void OnQuit()
        {            
            SaveProgramFilesDirectory();
        }

        public static void ReadProgramFilesDirectory()
        {
            try
            {
                if (File.Exists(ProgramFilesDirectory))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    FileStream fileStream = new FileStream(ProgramFilesDirectory, FileMode.Open);

                    programData = (ProgramData)binaryFormatter.Deserialize(fileStream);
                    fileStream.Close();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error in ProgramFilesDirectory");
            }
        }

        //public static void GetModelFile

        public static void SaveProgramFilesDirectory()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(ProgramFilesDirectory, FileMode.OpenOrCreate);
            binaryFormatter.Serialize(fileStream, programData);
            fileStream.Close();
        }
        #endregion


        #region Template region



        public static bool HasTemplate(string templateName)
        {
            if (Directory.Exists(String.Concat(TemplateDirectory, "\\", templateName))) return true;
            return false;
        }

        /// <summary>
        /// Create a new one if the template name does not exist
        /// </summary>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public static string GetTemplate(string templateName)
        {
            string name;
            if ((name = GetTemplateFullPathViaName(templateName)) != null) return name;
            else
            {
                CreateAnEmptyTemplateDirectory(templateName);
                name = GetTemplate(templateName);
            }
            return name;
        }

        public static void CreateAnEmptyTemplateDirectory(string templateName)
        {
            string _templateDirectory = String.Concat(TemplateDirectory,"\\",templateName);
            if (Directory.Exists(_templateDirectory)) throw new System.Exception("Template that with the same name already exist");

            Directory.CreateDirectory(String.Concat(_templateDirectory, eDrawingDirectory));
            Directory.CreateDirectory(String.Concat(_templateDirectory, modelDirectory));
            Directory.CreateDirectory(String.Concat(_templateDirectory, cutDrawing));
            Directory.CreateDirectory(String.Concat(_templateDirectory, drawing2d));
        }

        public static string[] TemplateDirectories { get
            {
                return Directory.GetDirectories(TemplateDirectory);
            }
        }

        public static string[] Templates
        {
            get
            {
                List<string> names = new List<string>();
                foreach (var path in Directory.GetDirectories(TemplateDirectory))
                {
                    names.Add(Path.GetFileName(path));
                }
                return names.ToArray();
            }
        }

        public static string GetTemplateFullPathViaName(string name)
        {
            foreach (var path in Directory.GetDirectories(TemplateDirectory))
            {
                if (Path.GetFileName(path) == name) return path;
            }
                        
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">the template name</param>
        /// <returns></returns>
        public static string GetTemplateDataFilePathViaName(string name)
        {
            foreach (var path in Directory.GetDirectories(TemplateDirectory))
            {
                if (Path.GetFileName(path) == name) {
                    return String.Concat(path,"//",name,".dat");
                }
            }

            return null;
        }        

        public static void CreateANewTemplate(string templateName)
        {

        }
        #endregion


        #region Project region

        public static void ClearTemporaryProjectDirectory()
        {
            ClearFiles(TemporaryProjectDirectory);
        }

        public static void ClearTemporary2DCutingFileFolder()
        {
            ClearFiles(TemporaryCutingDrawingsDirectory);
            ClearFiles(TemporaryCutingDrawingDirectory);
        }

        private static void ClearFiles(string directoryPath)
        {
            foreach (string filePath in Directory.GetFiles(directoryPath))
            {
                File.Delete(filePath);
            }
        }
        #endregion
    }
}
