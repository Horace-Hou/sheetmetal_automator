﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Globalization;

using WpfApp1.Script;



namespace WpfApp1
{
    public static class ExtensionClass
    {
        /// <summary>
        /// Returns true if string is numeric and not empty or null or whitespace.
        /// Determines if string is numeric by parsing as Double
        /// </summary>
        /// <param name="str"></param>
        /// <param name="style">Optional style - defaults to NumberStyles.Number (leading and trailing whitespace, leading and trailing sign, decimal point and thousands separator) </param>
        /// <param name="culture">Optional CultureInfo - defaults to InvariantCulture</param>
        /// <returns></returns>
        public static bool IsNumeric(this string str, NumberStyles style = NumberStyles.Number,
            CultureInfo culture = null)
        {
            double num;
            if (culture == null) culture = CultureInfo.InvariantCulture;
            return Double.TryParse(str, style, culture, out num) && !String.IsNullOrWhiteSpace(str);
        }

        public static bool IsPositiveNumeric(this string str, NumberStyles style = NumberStyles.Number,
            CultureInfo culture = null)
        {
            double num;
            if (culture == null) culture = CultureInfo.InvariantCulture;
            if(Double.TryParse(str, style, culture, out num) && !String.IsNullOrWhiteSpace(str))
            {
                if (num >= 0) return true;
            }
            return false;
        }

        public static double ToDouble(this string str)
        {
            double num;
            if(Double.TryParse(str, out num) && !String.IsNullOrWhiteSpace(str))
            {
                return num;
            }
            throw new System.Exception("Can't cast to double");
        }
        ///// <summary>
        ///// This extension method will search recursively for child elements of the desired type
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="depObj"></param>
        ///// <returns></returns>
        public static T GetChildOfType<T>(this DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }
            return null;
        }

        ///// <summary>
        ///// This extension method will search recursively for child elements of the desired type
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="depObj"></param>
        ///// <returns></returns>
        public static List<T> GetChildrenOfType<T>(this DependencyObject depObj) where T : DependencyObject
        {
            List<T> result = new List<T>();
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                if ((child as T) != null) result.Add(child as T);
                result.AddRange(GetChildrenOfType<T>(child));
            }
            return result;
        }

        /// <summary>
        /// this function get the wanted ancestor by type until first match being found
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="child"></param>
        /// <returns></returns>
        public static T GetAncestorOfType<T>(FrameworkElement child) where T : FrameworkElement
        {
            var parent = VisualTreeHelper.GetParent(child);
            if (parent != null && !(parent is T))
            {
                return (T)GetAncestorOfType<T>((FrameworkElement)parent);
            }
            return (T)parent;
        }

        /// <summary>
        /// this function get the wanted ancestor by type until first match being found
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="child"></param>
        /// <returns></returns>
        public static T GetAncestorOfType<T>(this DependencyObject child) where T : DependencyObject
        {
            var parent = VisualTreeHelper.GetParent(child);
            if (parent != null && !(parent is T))
            {
                return (T)GetAncestorOfType<T>(parent);
            }
            return (T)parent;
        }

        public static string[] GetEdrawing(this string templateName)
        {
            string[] files = new string[] { };
            string dir = String.Concat(AppManager.GetTemplateFullPathViaName(templateName), AppManager.eDrawingDirectory);
            if (Directory.Exists(dir))
            {
                 files = Directory.GetFiles(dir);
            }
            return files;
        }

        public static string GetTemplateDirectory(this string templateName)
        {
            return AppManager.GetTemplateFullPathViaName(templateName);
        }

        public static string GetDrawing2dDirectory(this string templateDirectory)
        {
            string str = String.Concat(templateDirectory, AppManager.drawing2d);
            if (!Directory.Exists(str)) throw new System.Exception("The target drawing2d directory does not exist.");

            return str;
        }

        public static string GetModelDirectory(this string templateDirectory)
        {
            string str = String.Concat(templateDirectory, AppManager.modelDirectory);
            if (!Directory.Exists(str)) throw new System.Exception("The target modelDirectory directory does not exist.");

            return str;
        }

        public static string GetModelMainAssem(this string templateName)
        {
            string str = String.Concat(templateName.GetTemplateDirectory().GetModelDirectory(), "\\Assem1.SLDASM");
            if (File.Exists(str)) return str;
            else return null;
        }



        public static string GetModelMainAssemInThatDirectory(this string directory)
        {
            string str = String.Concat(directory, "\\Assem1.SLDASM");
            if (File.Exists(str)) return str;
            else return null;
        }

        public static string GetEdrawingDirectory(this string templateDirectory)
        {
            string str = String.Concat(templateDirectory, AppManager.eDrawingDirectory);
            if (!Directory.Exists(str)) throw new System.Exception("The target modelDirectory directory does not exist.");

            return str;
        }

        public static string GetCutDrawingDirectory(this string templateDirectory)
        {
            string str = String.Concat(templateDirectory, AppManager.cutDrawing);
            if (!Directory.Exists(str)) throw new System.Exception("The target cutDrawing directory does not exist.");

            return str;
        }

        public static T GetSerialisedDataByFilePath<T>(this string path)
        {
            if (!File.Exists(path))
            {
                throw new System.Exception("Data file not found, path: " + path);
            }
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(path, FileMode.Open);
            T data = (T)binaryFormatter.Deserialize(fileStream);
            fileStream.Close();
            return data;
        }        

        public static bool TrySaveSerialisedDataByFilePath<T>(this T data,string TargetPath)
        {
            try
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream fileStream = new FileStream(TargetPath, FileMode.OpenOrCreate);
                binaryFormatter.Serialize(fileStream, data);
                fileStream.Close();
            }catch(System.Exception ex)
            {
                return false;
            }

            return true;
        }

        public static void LoadSerialisedDataToTemplate(this string path, ref Template template)
        {
            AppTemplateData dat = path.GetSerialisedDataByFilePath<AppTemplateData>();

            foreach (TemplateDimensionData data in dat.SavedDimensionDataList)
            {
                template.Dimensions.Add(new DimensionData(data.Dimensions, data.DimensionFormula, data.DimensionName,true));
            }
            foreach (var data in dat.TemplateInputDataList)
            {
                template.ProjectInputs.Add(new InputData(data.InputName,
                    data.InputDefaultValue,
                    false,
                    data.MinimumValue,
                    data.MaximumValue));
                Console.WriteLine("Input added");
            }
            template.FilePath = dat.TemplateName.GetModelMainAssem();
            template.Name = dat.TemplateName;
            template.DescriptionBox = dat.Description;

            Console.WriteLine("Template file path: " + template.FilePath + ", template name is: " + template.Name);

        }

        public static double ToMeter(this double value)
        {
            return value / 1000;
        }

        public static double ToMiliMeter(this double value)
        {
            return value * 1000;
        }

        public static string GetTemplateFileFullPath(this string templateName)
        {
            if (AppManager.HasTemplate(templateName))
            {
                return AppManager.GetTemplateFullPathViaName(templateName);
            }
            return null;
        }
    }
}