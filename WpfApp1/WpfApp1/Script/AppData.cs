﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using SolidWorks.Interop.swconst;


namespace WpfApp1.Script
{
    public class AppData
    {
    }

    /// <summary>
    /// The templatedata structure for store and transmitting
    /// </summary>
    [System.Serializable]
    public class AppTemplateData
    {
        public string TemplateMainAssemPath;
        public string TemplateName;
        public string TemplateID;
        public string TemplateCreationDate;
        public string TemplateCreator;
        public List<TemplateEditLogData> EditHistory = new List<TemplateEditLogData>();

        public List<TemplateComponentData> TemplateComponentDataList;
        public List<TemplateInputData> TemplateInputDataList;
        public List<TemplateDimensionData> SavedDimensionDataList;

        public string Description;

        public AppTemplateData(List<TemplateComponentData> componentDataList, List<TemplateInputData> templateInputDataList, List<TemplateDimensionData> savedDimensionDataList)
        {
            TemplateComponentDataList = componentDataList;
            TemplateInputDataList = templateInputDataList;
            SavedDimensionDataList = savedDimensionDataList;
        }
    }


    [System.Serializable]
    public class TemplateEditLogData
    {
        public DateTime DateTime;
        public string Editor;
        public TemplateEditLogData(DateTime dateTime, string editor)
        {
            DateTime = dateTime;
            Editor = editor;
        }
    }

    [System.Serializable]
    public class TemplateDimensionData
    {
        public string DimensionName;
        public string DimensionFormula;
        public List<string> Dimensions = new List<string>();
        public TemplateDimensionData(string dimensionName, string dimensionFormula, List<string> dimensions)
        {
            DimensionName = dimensionName;
            DimensionFormula = dimensionFormula;
            Dimensions = dimensions;
        }
    }

    [System.Serializable]
    public class TemplateInputData
    {
        public string InputName;
        public double InputDefaultValue;
        public double MinimumValue;
        public double MaximumValue;
        public TemplateInputData(string inputName, double inputDefaultValue, double minimumValue, double maximumValue)
        {
            InputName = inputName;
            InputDefaultValue = inputDefaultValue;
            MinimumValue = minimumValue;
            MaximumValue = maximumValue;
        }
    }

    [System.Serializable]
    public class TemplateComponentData
    {
        public string ComponentName;
        public swDocumentTypes_e Type;
        public List<string> Dimensions;

        public TemplateComponentData(string componentName, swDocumentTypes_e type, List<string> dimensions)
        {
            ComponentName = componentName;
            Type = type;
            Dimensions = dimensions;
        }
    }


    [System.Serializable]
    public class ProgramData
    {
        public string AutoCADExe_FilePath;
        public DateTime LastEditDate;
    }
}
