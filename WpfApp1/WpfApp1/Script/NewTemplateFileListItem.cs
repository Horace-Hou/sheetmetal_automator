﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.ComponentModel;
//using System.Collections.ObjectModel;
////using System.Windows.Controls;
////using System.Drawing;
//using System.Windows.Media.Imaging;
//using System.Windows.Controls;
//using System.Windows;

//namespace WpfApp1
//{    
//    public class NewTemplateFileListItem
//    {
//        public BitmapImage Icon{get;set;}
//        public string FileName { get; set; }
//        public NewTemplateFileListItem(BitmapImage _Icon, string _FileName)
//        {
//            Icon = _Icon;
//            FileName = _FileName;
//        }
//    }

//     public class NewTemplateFileListModel:INotifyPropertyChanged
//    {
//        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };
//        private ObservableCollection<NewTemplateFileListItem> thisData = new ObservableCollection<NewTemplateFileListItem>();
//        public ObservableCollection<NewTemplateFileListItem> ThisData {
//            get { return thisData; }
//            set
//            {
//                thisData = value;
//                PropertyChanged(this, new PropertyChangedEventArgs("ThisData"));
//            }
//        }
//        readonly BitmapImage Solidworks_Icon = new BitmapImage(new Uri("pack://application:,,,/Images/Solidworks_Icon.png"));
//        readonly BitmapImage Solidworks_PartIcon = new BitmapImage(new Uri("pack://application:,,,/Images/Solidworks_PartIcon.png"));

//        public void AddSolidworksPart(string _context)
//        {
//            thisData.Add(new NewTemplateFileListItem(Solidworks_PartIcon, _context));
//        }
//        public void AddSolidworksAssembly(string _context)
//        {
//            thisData.Add(new NewTemplateFileListItem(Solidworks_Icon, _context));
//        }        
//        public void ClearAll()
//        {
//            ThisData.Clear();
//        }
//    }  
//}
