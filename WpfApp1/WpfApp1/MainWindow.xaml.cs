﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Threading;

using Microsoft.WindowsAPICodePack.Dialogs;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;

using Jace;
//using System.Windows.Forms;

using eDrawingHostControl;

using WpfApp1.Script;

namespace WpfApp1
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public Template theTemplate = new Template("", "");

        private Dictionary<string, Panel> mainPanelsControl = new Dictionary<string, Panel>();

        #region InitFunctions

        public MainWindow()
        {

            InitializeComponent();
            start();
        }


        public void start()
        {
            // set up panel control list
            InitialiseThePanelControl();

            //set up the data binding
            SetUpBinding();

            // solidworks modelview
            SetUpEdrawingsModelViewControl();

            theTemplate.State = TemplateState.TemplatePreview;

            // load the program data
            AppManager.OnStart();
        }


        private void InitialiseThePanelControl()
        {
            mainPanelsControl.Add("templateGrid", templateGrid);
            mainPanelsControl.Add("SelectTemplateMenu", SelectTemplateMenu);            
        }
        /// <summary>
        /// All the data binding init here
        /// </summary>
        private void SetUpBinding()
        {
            NewTemplateFileListViewObj.DataContext = theTemplate;
            TemplatePage_TabControl0_ParameterTab_DimensionList.DataContext = theTemplate;
            TemplatePage_TabControl0_ParameterTab_InputList.DataContext = theTemplate;
            TemplateNameTextBox.DataContext = theTemplate;
            TemplatePathTextBox.DataContext = theTemplate;
            TemplatesPreviewNameListView.DataContext = theTemplate;
            TemplatePathConfirm.DataContext = theTemplate;
            TemplatePathClear.DataContext = theTemplate;
            BrowseButton.DataContext = theTemplate;
            TemplatePreviewButtonControlGrid.DataContext = theTemplate;
            TemplatePreviewNameListGrid.DataContext = theTemplate;
            ColumnDefinition_1.DataContext = theTemplate;
            ColumnDefinition_2.DataContext = theTemplate;
            ProjectInputsGrid.DataContext = theTemplate;
            DescriptionBox.DataContext = theTemplate;
            MarkPointOptionComboBox.DataContext = theTemplate;
            PreviewEdrawingRollForwardButton.DataContext = theTemplate;
            PreviewEdrawingRollBackButton.DataContext = theTemplate;
            TemplateDescription_TemplateEditPanel.DataContext = theTemplate;
            PreviewEdrawingRollForwardButton2.DataContext = theTemplate;
            PreviewEdrawingRollBackButton2.DataContext = theTemplate;
            WindowsFormsHost_ModelView.DataContext = theTemplate;
            TemplateSelectionPanel_ModelPreview.DataContext = theTemplate;
            Flyout1.DataContext = theTemplate;
            FunctionGrid1.DataContext = theTemplate;
            ProjectInputsGrid0.DataContext = theTemplate;
            TemplatePreviewNameListGrid0.DataContext = theTemplate;
            //theTemplate.Flyout1Heading = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            theTemplate.Flyout1Heading = String.Concat("Welcome, ", Environment.UserName, ".");

            theTemplate.Flyout1IsOpen = true;
            LeftSideMenuRectangle.MouseEnter += (sender, e) => {
                if (!theTemplate.Flyout1IsOpen)
                {
                    theTemplate.Flyout1IsOpen = true;
                }
            };

            UserIcon.DataContext = theTemplate;

            theTemplate.UserIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/if_users-15_984108.png"));            

            ChangeUserIcon.MouseLeftButtonDown += (sender, e) => {
                //throw new NotImplementedException("This has not been implemented");
                Console.WriteLine("This has not been implemented.");
            };

            // username
            UserNameTextbox.DataContext = theTemplate;
            theTemplate.UserName = "UserName";
            UserNameTextbox.Foreground = Brushes.Gray;
            UserNameTextbox.PreviewMouseDown += OnPreviewUserNameDown;
            UserNameTextbox.PreviewKeyDown += OnPreviewUserNameDown;
            UserNameTextbox.LostFocus += (sender, e) =>
            {
                Console.WriteLine("on user name exit");
                if (theTemplate.UserName == "")
                {
                    theTemplate.UserName = "UserName";
                    UserNameTextbox.Foreground = Brushes.Gray;
                }
            };

            PasswordShowToggle.DataContext = theTemplate;
            theTemplate.PasswordShow = false;

            //PasswordTextbox             
            PasswordShowToggle.Checked += (sender, e) =>
            {
                ShowHidePassword.Content = "Hide";
                PasswordTextbox.Text = PasswordPasswordbox.Password;
                PasswordTextbox.Visibility = Visibility.Visible;
                PasswordPasswordbox.Visibility = Visibility.Collapsed;
            };
            PasswordShowToggle.Unchecked += (sender, e) =>
            {
                ShowHidePassword.Content = "Show";
                PasswordPasswordbox.Password = PasswordTextbox.Text;
                PasswordTextbox.Visibility = Visibility.Collapsed;
                PasswordTextbox.Text = "";
                PasswordPasswordbox.Visibility = Visibility.Visible;
            };

            theTemplate.LoginState = false;
            LoginButton.Click += (sender, e) =>
            {
                theTemplate.LoginState = false;
                foreach(var valPair in AppManager.accountInfo)
                {
                    if(theTemplate.UserName == valPair.Key)
                    {
                        if(PasswordPasswordbox.Password == valPair.Value)
                        {
                            theTemplate.LoginState = true;
                            OnLoginSuccessful(valPair.Key);
                        }
                    }
                }
            };

            Closing += OnWindowClosing;
        }

        private void OnLoginSuccessful(string username)
        {
            UserGroupBox.Visibility = Visibility.Visible;
            LoginGroupBox.Visibility = Visibility.Collapsed;
            theTemplate.Flyout1Heading = String.Concat("Welcome, ", username, ".");
            // free the flyout
            Flyout1.MouseLeave += (sender, e) =>
            {
                if (theTemplate.Flyout1IsOpen)
                {
                    theTemplate.Flyout1IsOpen = false;
                }
            };
        }

        private void OnPreviewUserNameDown(object sender, object e)
        {
            if (theTemplate.UserName == "UserName")
            {
                theTemplate.UserName = "";
                UserNameTextbox.Foreground = Brushes.White;
            }
        }

        private void OnProjectConfirmAllInputsButtonClick(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Confirm clicked");
            foreach(var input in theTemplate.ProjectInputs)
            {
                input.IsConfirmed = true;                
            }
        }

        private void OnProjectEditAllInputsButtonClick(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Edit clicked");
            foreach (var input in theTemplate.ProjectInputs)
            {
                input.IsConfirmed = false;
            }
        }

        private void SetUpEdrawingsModelViewControl()
        {
            WindowsFormsHost_ModelView.Child = theTemplate.TemplateEdrawingControl1;
            TemplateSelectionPanel_ModelPreview.Child = theTemplate.TemplateEdrawingControl2;
        }
        #endregion


        #region Uitilities

        private void SetUpTheCurrentPanel(string panelName)
        {
            foreach (var panel in mainPanelsControl)
            {
                if (panel.Key == panelName) { panel.Value.Visibility = Visibility.Visible; Console.WriteLine("panel found"); }
                else panel.Value.Visibility = Visibility.Collapsed;
            }
        }

        private void Refresh()
        {
            switch (theTemplate.State)
            {
                case TemplateState.NewTemplate:

                    break;
                case TemplateState.EditTemplate:
                    theTemplate.SetUpNewEdrawingPath(theTemplate.PreviewEdrawingItems[theTemplate.PreviewEdrawingItemIndex]);

                    break;
                case TemplateState.TemplatePreview:
                    
                    RefreshTheTemplateList();
                    break;
                case TemplateState.CreateANewProject:
                    SetUpProjectPage();
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Template select preview

        private void SelectTemplateMenu_Loaded(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Onloaded");
            theTemplate.SetTemplateNames(AppManager.Templates);

        }

        private void SelectTemplateMenu_Unloaded(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Unloaded");

        }

        private void SelectTemplateMenu_CloseButtonClicked(object sender, RoutedEventArgs e)
        {
            
            //SelectTemplateMenu.Visibility = Visibility.Collapsed;
        }

        private void RefreshTheTemplateList(object seder, RoutedEventArgs e)
        {
            theTemplate.SetTemplateNames(AppManager.Templates);
        }

        private void RefreshTheTemplateList()
        {
            theTemplate.SetTemplateNames(AppManager.Templates);
        }

        private void TemplatesPreviewNameListViewItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string[] strs = ((ListViewItem)sender).Content.ToString().GetEdrawing();
                if (strs.Length == 0) return;
                //if (strs.Length == 0) eDrawingControl_TemplatePreview.eDrawingControlWrapper.CloseActiveDoc(eDrawingControl_TemplatePreview.eDrawingControlWrapper.FileName);
                theTemplate.PreviewEdrawingItems.Clear();
                //theTemplate.PreviewEdrawingItems.AddRange(strs.ToList());
                foreach (string str in strs)
                {
                    theTemplate.PreviewEdrawingItems.Add(str);
                }
                theTemplate.PreviewEdrawingItemIndex = theTemplate.PreviewEdrawingItems.Count - 1;
                Console.WriteLine("The template1 index is : " + theTemplate.PreviewEdrawingItemIndex);
                Console.WriteLine("The template2 index is : " + (theTemplate.PreviewEdrawingItems.Count));
                theTemplate.DescriptionBox = "";
                //Console.WriteLine("The Length is : " + strs.Length);
                if (strs.Length > 0) theTemplate.SetUpNewEdrawingPath(theTemplate.PreviewEdrawingItems[theTemplate.PreviewEdrawingItemIndex]);
                //if (strs.Length > 0) theTemplate.SetUpNewEdrawingPath(theTemplate.PreviewEdrawingItems[0]);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Data.ToString());
            }
        }

        private void PreviewEdrawingRollForwardClick(object sender, RoutedEventArgs e)
        {
            theTemplate.PreviewEdrawingItemIndex++;
        }

        private void PreviewEdrawingRollBackClick(object sender, RoutedEventArgs e)
        {
            theTemplate.PreviewEdrawingItemIndex--;
        }

        private void TemplatePreviewConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            theTemplate.State = TemplateState.CreateANewProject;
            Refresh();            
        }

        private void TemplatePreviewEditButton_Click(object sender, RoutedEventArgs e)
        {
            string templateName = (string)TemplatesPreviewNameListView.SelectedItem;
            string modelFileName = theTemplate.TemplateEdrawingControl1.eDrawingControlWrapper.FileName;
            
            EditTemplate(templateName, modelFileName);

        }

        private void LoadTheTemplateFromPreviewToEdit()
        {
            SetUpTheCurrentPanel("templateGrid");
        }
       
        #endregion

        #region TemplatePanel
        private void Template_Path_inputbox_Copy_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Template_name_inputbox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }        

        private void TemplatePage_TabControl0_ParameterTab_InputList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_RemoveAnEmptyDimension_Click(object sender, RoutedEventArgs e)
        {
            if (theTemplate != null) theTemplate.RemoveLastDimension();
        }

        private void Button_AddADimension_Click(object sender, RoutedEventArgs e)
        {
            if (theTemplate != null && theTemplate.selectedComponent != null) theTemplate.AddADimension(theTemplate.selectedComponent.Dimension, "");
        }

        private void Button_AddAnInput_Click(object sender, RoutedEventArgs e)
        {
            if (theTemplate != null) theTemplate.AddAnInput("", 1);
        }

        private void Button_RemoveAnEmptyInput_Click(object sender, RoutedEventArgs e)
        {
            if (theTemplate != null) theTemplate.RemoveLastInput();
        }

        private void CreateANewTemplateButton_Click(object sender, RoutedEventArgs e)
        {
            SetUpTheCurrentPanel("templateGrid");
            theTemplate.State = TemplateState.NewTemplate;
            theTemplate.ClearAll();
        }

        private void ButtonClick_EditTemplate(object sender, RoutedEventArgs e)
        {
            SetUpTheCurrentPanel("SelectTemplateMenu");
        }

        private void ButtonClick_CollpaseTemplatePanel(object sender, RoutedEventArgs e)
        {            
            Task task = new Task(async () => {
                theTemplate.SLDEModelVisibility = Visibility.Hidden;
                var settings = new MetroDialogSettings();
                var result = await this.ShowMessageAsync("Message", "Are you sure to exit?", MessageDialogStyle.AffirmativeAndNegative);                
                if (result == MessageDialogResult.Affirmative)
                {
                    SetUpTheCurrentPanel("SelectTemplateMenu");
                    RestoreTheTemplatePage();
                    theTemplate.SLDEModelVisibility = Visibility.Visible;
                }
            });
            task.RunSynchronously();
        }

        private void ButtonClick_DeleteTemplate(object sender, RoutedEventArgs e)
        {

        }

        private void LoadTemplate(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(TemplatePathTextBox.Text)) { ShowMessage_WithEModelVisibility("Message", "The Selected assembly file path do not exist");return; }
            //{ System.Windows.MessageBox.Show("The Selected assembly file path do not exist"); return; }
            if (AppManager.HasTemplate(TemplateNameTextBox.Text)) { ShowMessage_WithEModelVisibility("Message", "Template name not valid."); return; }
            //{ System.Windows.MessageBox.Show("Template name not valid."); return; }
            if (!SolidworksManager.TryLoadTemplate(ref theTemplate, TemplatePathTextBox.Text))
            {
                Console.WriteLine("Load the template failed");
                RestoreTheTemplatePage();
            }
        }

        private void BrowseTemplateFilePath(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                theTemplate.FilePath = openFileDialog.FileName;
            }
        }

        private void RestoreTheTemplatePage(object sender, RoutedEventArgs e)
        {
            theTemplate.ClearAll();
            TemplatePathTextBox.Text = "";
            TemplateNameTextBox.Text = "";
        }

        private void RestoreTheTemplatePage()
        {
            theTemplate.ClearAll();
            TemplatePathTextBox.Text = "";
            TemplateNameTextBox.Text = "";
            theTemplate.CloseTemplateEdrawing();
        }
        private void SolidworksVisible(object sender, RoutedEventArgs e)
        {
            SolidworksManager.Visiblity = true;
        }

        private void SolidworksHide(object sender, RoutedEventArgs e)
        {
            SolidworksManager.Visiblity = false;
        }

        private void NewTemplateFileListViewObjEvent_mouseUp(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Controls.ListViewItem item = sender as System.Windows.Controls.ListViewItem;

            ComponentsData data = (ComponentsData)item.Content;
            theTemplate.selectedComponent = data;
        }

        private void OnClick_DimensionConfirmButton(object sender, RoutedEventArgs e)
        {
            //leave the math function here

            ListViewItem listViewItem = ExtensionClass.GetAncestorOfType<ListViewItem>((Button)sender);

            // validate
            if (!ValidateTheDimenionFormulaInput(((DimensionData)listViewItem.DataContext).Formula))
            {
                return;
            }

            listViewItem.GetChildOfType<ComboBox>().IsEnabled = false;
            listViewItem.GetChildOfType<TextBox>().IsEnabled = false;
            
            ((DimensionData)listViewItem.DataContext).IsConfirmed = true;
            ((DimensionData)listViewItem.DataContext).IsNotConfirmed = false;
        }

        private void OnClick_DimensionEditButton(object sender, RoutedEventArgs e)
        {
            ListViewItem listViewItem = ExtensionClass.GetAncestorOfType<ListViewItem>((Button)sender);
            listViewItem.GetChildOfType<ComboBox>().IsEnabled = true;
            listViewItem.GetChildOfType<TextBox>().IsEnabled = true;

            ((DimensionData)listViewItem.DataContext).IsConfirmed = false;
            ((DimensionData)listViewItem.DataContext).IsNotConfirmed = true;
        }


        private void OnClick_DimensionDeleteButton(object sender, RoutedEventArgs e)
        {
            if (!theTemplate.Dimensions.Remove((DimensionData)(((Button)sender).GetAncestorOfType<ListViewItem>().GetChildOfType<ComboBox>().DataContext)))
            {
                throw new System.Exception("Remove dimension failed");
            }
        }


        private void OnClick_InputConfirmButton(object sender, RoutedEventArgs e)
        {
            ListViewItem listViewItem = ExtensionClass.GetAncestorOfType<ListViewItem>((Button)sender);

            // validate


            List<TextBox> items = listViewItem.GetChildrenOfType<TextBox>();
            TextBox name = new TextBox();
            TextBox input = new TextBox();

            foreach (var item in items)
            {
                if (item.Name == "inputItemTextBox1")
                {
                    if (!ValidateTheInputFormulaInput(item.Text))
                    {
                        return;
                    }
                    name = item;
                }
                if (item.Name == "DefaultValue") input = item;
            }

            //theTemplate.Inputs.Add(new InputData(name.Text,Convert.ToDouble(input.Text)));
            name.IsEnabled = false;
            input.IsEnabled = false;

            ((InputData)listViewItem.DataContext).IsConfirmed = true;
            ((InputData)listViewItem.DataContext).IsNotConfirmed = false;
        }


        /// <summary>
        /// when the input edit button is clicked, activate the fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClick_InputEditButton(object sender, RoutedEventArgs e)
        {
            ListViewItem listViewItem = ExtensionClass.GetAncestorOfType<ListViewItem>((Button)sender);
            List<TextBox> items = listViewItem.GetChildrenOfType<TextBox>();
            TextBox name = new TextBox();
            TextBox input = new TextBox();

            foreach (var item in items)
            {
                if (item.Name == "inputItemTextBox1") name = item;
                if (item.Name == "DefaultValue") input = item;
            }
            name.IsEnabled = true;
            input.IsEnabled = true;

            ((InputData)listViewItem.DataContext).IsConfirmed = false;
            ((InputData)listViewItem.DataContext).IsNotConfirmed = true;
        }



        private void OnClick_InputDeleteButton(object sender, RoutedEventArgs e)
        {
            ListViewItem listViewItem = ExtensionClass.GetAncestorOfType<ListViewItem>((Button)sender);
            List<TextBox> items = listViewItem.GetChildrenOfType<TextBox>();
            TextBox name = new TextBox();
            TextBox input = new TextBox();

            foreach (var item in items)
            {
                if (item.Name == "inputItemTextBox1") name = item;
                if (item.Name == "DefaultValue") input = item;
            }
            foreach (InputData data in theTemplate.Inputs)
            {
                Console.WriteLine("the data name is " + data.Name);
                if (data.Name == name.Text)
                {
                    Console.WriteLine("Equal");
                    theTemplate.Inputs.Remove(data);
                    return;
                }
            }
        }


        private void LostFocus_inputItemTextBox1(object sender, RoutedEventArgs e)
        {
            string text = ((TextBox)sender).Text;
            int i = 0;

            if (text == "") {
                ShowMessage_WithEModelVisibility("Message", "The input name can't be empty.");
                    //MessageBox.Show("The input name can't be empty");
                return; }

            if (!Char.IsLetter(((TextBox)sender).Text.ToCharArray()[0]))
            {
                ShowMessage_WithEModelVisibility("Message", String.Concat(text, ": this input name is invalid, please start the input name with a letter"));
                //MessageBox.Show(text + ": this input name is invalid, please start the input name with a letter");
                ((TextBox)sender).Text = "";
                return;
            }

            foreach (var val in theTemplate.Inputs)
            {
                if (text == val.Name)
                {
                    i++;
                }
                if (i > 1)
                {
                    ShowMessage_WithEModelVisibility("Message", String.Concat(text, ": this input name already exist, please change a name"));
                    //MessageBox.Show(text + ": this input name already exist, please change a name");
                    ((TextBox)sender).Text = "";
                    return;
                }
            }
        }


        private void LostFocus_inputItemTextBox2(object sender, RoutedEventArgs e)
        {
            double result;
            if (double.TryParse(((TextBox)sender).Text, out result))
            {
                if (result >= 0) return;
            }
            ShowMessage_WithEModelVisibility("Message", "default value invalid");
            //MessageBox.Show("default value invalid");
            ((TextBox)sender).Text = "";
        }


        private void LostFocus_TemplateFormula(object sender, RoutedEventArgs e)
        {
            Dictionary<string, double> variables = new Dictionary<string, double>();
            CalculationEngine engine = new CalculationEngine();
            double result;
            if (((TextBox)sender).Text == "")
            {
                //ShowMessage_WithEModelVisibility("Message", "The formula textbox can't be empty.");
                MessageBox.Show("The formula textbox can't be empty."); return;
            }
            foreach (InputData inputdata in theTemplate.Inputs)
            {
                variables.Add(inputdata.Name, inputdata.DefaultValue);
            }
            try
            {
                result = engine.Calculate(((TextBox)sender).Text, variables);
                Console.WriteLine("result is " + result);
            }
            catch (Exception ex)
            {
                //ShowMessage_WithEModelVisibility("Message", "The formula has unspecified variables");
                MessageBox.Show("The formula has unspecified variables");
                ((TextBox)sender).Text = "";
            }
        }

        private void EditTemplate(string templateName, string modelFileName)
        {
            SetUpTheCurrentPanel("templateGrid");
            
            TemplateNameTextBox.Text = templateName;
            TemplatePathTextBox.Text = "path not available";

            string templateFilePath = AppManager.GetTemplateDataFilePathViaName(templateName);
            if (templateFilePath == null)
            {
                ShowMessage_WithEModelVisibility("Message", "Cant find the template");

                return;
            }

            theTemplate.State = TemplateState.EditTemplate;
            AppTemplateData data = templateFilePath.GetSerialisedDataByFilePath<AppTemplateData>();
            theTemplate.ReSetProperties(data.TemplateName,
                data.TemplateMainAssemPath,
                data.TemplateComponentDataList,
                data.SavedDimensionDataList,
                data.TemplateInputDataList, true);
            theTemplate.DescriptionBox = data.Description;
            theTemplate.FilePath = templateName.GetModelMainAssem();
            Refresh();
        }

        private void SaveTheTemplate(object sender, RoutedEventArgs e)
        {
            bool SaveSucceed = false;
            switch (theTemplate.State)
            {
                case TemplateState.NewTemplate:
                    SaveSucceed = SaveNewTemplate();
                    break;
                case TemplateState.EditTemplate:
                    SaveSucceed = SaveEditedTemplate();
                    break;
                default:
                    break;
            }
            //if (SaveSucceed)
            //{
            //    SetUpTheCurrentPanel("SelectTemplateMenu");
            //    RestoreTheTemplatePage();
            //}
        }

        private bool SaveEditedTemplate()
        {
            List<TemplateDimensionData> dimensionsDataList = GetDimensionList();
            List<TemplateComponentData> componentDataList = GetComponentDataList();
            List<TemplateInputData> inputDataList = GetInputDataList();
            AppTemplateData appTemplateData = new AppTemplateData(componentDataList, inputDataList, dimensionsDataList);
            appTemplateData.TemplateName = theTemplate.Name;
            appTemplateData.TemplateMainAssemPath = theTemplate.FilePath;
            appTemplateData.Description = theTemplate.DescriptionBox;

            if (appTemplateData.TrySaveSerialisedDataByFilePath(AppManager.GetTemplateDataFilePathViaName(theTemplate.Name)))
            {
                ShowMessage_WithEModelVisibility("Message", "File saved successful");
                return true;
            }
            return false;
        }

        private bool SaveNewTemplate()
        {
            Console.WriteLine("1 : " + theTemplate.FilePath);
            if (!IsDimensionsAndInputsConfirmed()) return false;
            if (AppManager.HasTemplate(theTemplate.Name))
            {
                ShowMessage_WithEModelVisibility("Message", "The template name already exist, please change a templateName");
                return false;
            }
            else
            {
                // create a new template folder with the name
                AppManager.CreateAnEmptyTemplateDirectory(theTemplate.Name);
            }
            string dir = AppManager.GetTemplateFullPathViaName(theTemplate.Name);
            if (dir == null) throw new Exception("Can't find the template directory with the name");
            string path = String.Concat(dir, "\\", theTemplate.Name, ".dat");

            //theTemplate.Name = TemplatePathTextBox.Text;
            List<TemplateDimensionData> dimensionsDataList = GetDimensionList();
            List<TemplateComponentData> componentDataList = GetComponentDataList();
            List<TemplateInputData> inputDataList = GetInputDataList();
            Console.WriteLine("2 : " + theTemplate.FilePath);
            AppTemplateData appTemplateData = new AppTemplateData(componentDataList, inputDataList, dimensionsDataList);
            appTemplateData.TemplateName = theTemplate.Name;
            appTemplateData.TemplateMainAssemPath = theTemplate.FilePath;
            appTemplateData.Description = theTemplate.DescriptionBox;

            FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, appTemplateData);
            fileStream.Close();

            Console.WriteLine("The template Main assemPath is, " + appTemplateData.TemplateMainAssemPath);
            Console.WriteLine("The template file path is" + theTemplate.FilePath);
            // save the solidworks file
            if (!SolidworksManager.TrySaveNewTemplateModelFileWithEdrawing(theTemplate.FilePath, appTemplateData))
            {
                ShowMessage_WithEModelVisibility("Message", "Sld files were not able to be saved");
                return false;
            }
            ShowMessage_WithEModelVisibility("Message", "File saved successful");
            return true;
        }

        private bool IsDimensionsAndInputsConfirmed()
        {
            ItemCollection dimensionItems = TemplatePage_TabControl0_ParameterTab_DimensionList.Items;
            foreach (DimensionData dimensionItem in dimensionItems)
            {
                if (dimensionItem.IsNotConfirmed)
                {
                    ShowMessage_WithEModelVisibility("Message", "Please confirm all the dimensions");
                    return false;
                }
            }
            return true;
        }

        private List<TemplateComponentData> GetComponentDataList()
        {
            List<TemplateComponentData> dataList = new List<TemplateComponentData>();

            foreach (ComponentsData data in theTemplate.Components)
            {
                dataList.Add(new TemplateComponentData(data.Name, data.sldDocumentType, data.Dimension));
            }
            return dataList;
        }

        private List<TemplateInputData> GetInputDataList()
        {
            List<TemplateInputData> dataList = new List<TemplateInputData>();

            foreach (InputData data in theTemplate.Inputs)
            {
                dataList.Add(new TemplateInputData(data.Name, data.DefaultValue, data.MinimumValue, data.MaximumValue));
            }
            return dataList;
        }


        private List<TemplateDimensionData> GetDimensionList()
        {
            List<TemplateDimensionData> dimensionList = new List<TemplateDimensionData>();

            foreach (DimensionData data in theTemplate.Dimensions)
            {
                dimensionList.Add(new TemplateDimensionData(data.SelectedDimension, data.Formula, data.Names));
            }
            return dimensionList;
        }

        private void SelectionChanged_TemplateDimension(object sender, SelectionChangedEventArgs args)
        {
            if ((string)((ComboBox)sender).SelectedItem != null)
            {
                //((DimensionData)((ComboBox)sender).DataContext).SelectedDimension = (string)((ComboBox)sender).SelectedItem;
            }
        }

        private bool ValidateTheDimenionFormulaInput(string dimensionFormula)
        {
            if (dimensionFormula == "")
            {
                ShowMessage_WithEModelVisibility("Message", "Dimension can ont be empty");
                //MessageBox.Show("Dimension cant be empty");
                return false;
            }
            return true;
        }

        private bool ValidateTheInputFormulaInput(string inputVariable)
        {
            if (inputVariable == "")
            {
                ShowMessage_WithEModelVisibility("Message", "Dimension can ont be empty");
                //MessageBox.Show("Input cant be empty");
                return false;
            }
            return true;
        }


        private void OnDimensionDropDownOpen(object sender, EventArgs e)
        {
            //should not be used
            //ComponentsData data = (ComponentsData)NewTemplateFileListViewObj.SelectedItem;
            //if(data == null)
            //{
            //    MessageBox.Show("Please select a component");
            //    return;
            //}
            //List<string> names = data.Dimension;
            //((DimensionData)((ComboBox)sender).GetAncestorOfType<ListViewItem>().DataContext).Names = names;
        }

        private void OnMinimumValueTextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            InputData data = ((InputData)((TextBox)sender).GetAncestorOfType<ListViewItem>().DataContext);
            if (!((TextBox)sender).Text.IsPositiveNumeric() 
                || data.MinimumValue > data.DefaultValue)
            {
                ShowMessage_WithEModelVisibility("Message", "Dimension can ont be empty");
                //MessageBox.Show("This has to be a number that is between 0 and default value.");
                ((TextBox)sender).Text = "";
            }
        }

        private void OnMaximumValueTextBoxBoxLostFocus(object sender, RoutedEventArgs e)
        {
            InputData data = ((InputData)((TextBox)sender).GetAncestorOfType<ListViewItem>().DataContext);
            if (!((TextBox)sender).Text.IsPositiveNumeric()
                || data.MaximumValue < data.DefaultValue)
            {
                ShowMessage_WithEModelVisibility("Message", "Dimension can ont be empty");
                //MessageBox.Show("This has to be a number that is between default value and positive infinity.");
                ((TextBox)sender).Text = "";
            }
        }
        #endregion

        #region Project
        private void OnProjectInputValueEnter(object sender, RoutedEventArgs e)
        {
            double result;
            bool valid = false;
            InputData inputData; 

            try
            {
                inputData = ((InputData)((TextBox)sender).GetAncestorOfType<ListViewItem>().DataContext);
                valid = double.TryParse(((TextBox)sender).Text, out result);
                if (!((TextBox)sender).Text.IsPositiveNumeric()) valid = false;
                if (!valid) valid = false;
                if (result < 0) valid = false;
                if (inputData.DefaultValue > inputData.MaximumValue || inputData.DefaultValue < inputData.MinimumValue) valid = false;
            }
            finally
            {
                if (!valid)
                {
                    ShowMessage_WithEModelVisibility("Message", "Dimension can ont be empty");
                    //MessageBox.Show("Default value invalid");
                    ((TextBox)sender).Text = "";
                }
            }   
        }

        private void OnProjectControlCancel(object sender, RoutedEventArgs e)
        {
            theTemplate.State = TemplateState.TemplatePreview;
            theTemplate.ClearAll();
            Refresh();
        }

        private async void OnProjectControlConfirm(object sender, RoutedEventArgs e)
        {
            try
            {
                theTemplate.SLDEModelVisibility = Visibility.Hidden;
                bool InputsConfirmed = true;
                foreach (var input in theTemplate.ProjectInputs)
                {
                    if (input.IsNotConfirmed)
                    {
                        InputsConfirmed = false; break;
                    }
                }
                Task t;
                if (InputsConfirmed)
                {
                    t = CreateAModel();
                }
                else
                {
                    t = ShowMessage("Message", "Please confirm all the inputs", MessageDialogStyle.Affirmative);
                }               

                await t.ContinueWith((T) =>
                {
                    Console.WriteLine("Thread in tc {0}", Thread.CurrentThread.ManagedThreadId);
                    theTemplate.SLDEModelVisibility = Visibility.Visible;
                });

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception found in " + ex.Data.ToString());
            }
        }

        private async Task ShowMessage(string title, string body, MessageDialogStyle messageDialogStyle = MessageDialogStyle.Affirmative)
        {
            Console.WriteLine("Thread in Run {0}", Thread.CurrentThread.ManagedThreadId);
            await this.ShowMessageAsync(title, body, messageDialogStyle);
        }

        public void ShowMessage_WithEModelVisibility(string title, string body, MessageDialogStyle messageDialogStyle = MessageDialogStyle.Affirmative)
        {
            Task task = new Task(async () => {
                theTemplate.SLDEModelVisibility = Visibility.Hidden;
                var settings = new MetroDialogSettings();

                await this.ShowMessageAsync(title, body, messageDialogStyle)
                .ContinueWith(t => { theTemplate.SLDEModelVisibility = Visibility.Visible; });
            });
            task.RunSynchronously();
        }

        private async Task CreateAModel()
        {
            Console.WriteLine("Thread in CraeteAModel {0}",Thread.CurrentThread.ManagedThreadId);
            ProgressDialogController progressDialogController = await this.ShowProgressAsync("Please wait patiently", "A Solidworks task is running.", false, new MetroDialogSettings { AnimateHide = false });
            progressDialogController.SetIndeterminate();

            bool result = await Task.Run(() =>
            {
                return SolidworksManager.TryCreatANewSolidworksModel(ref theTemplate);
            });
            await progressDialogController.CloseAsync();
            if (result)
            {
                await this.ShowMessageAsync("Message", "Successful", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }
            else
            {
                await this.ShowMessageAsync("Message", "Model creation failed", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }
        }

        //private void OnProjectControlConfirm(object sender, RoutedEventArgs e)
        //{
        //    foreach (var input in theTemplate.ProjectInputs)
        //    {
        //        if (input.IsNotConfirmed)
        //        {
        //            MessageBox.Show("Please confirm all the inputs");
        //            return;
        //        }
        //    }

        //    if (!SolidworksManager.TryCreatANewSolidworksModel(ref theTemplate))
        //    {
        //        MessageBox.Show("Model creation failed");
        //    }
        //    //theTemplate.State = TemplateState.TemplatePreview;
        //    //theTemplate.ClearAll();
        //    //Refresh();
        //}

        private void SetUpProjectPage()
        {
            string path = AppManager.GetTemplateDataFilePathViaName(theTemplate.SelectedTemplateName);
            Console.WriteLine("the name is : "+theTemplate.SelectedTemplateName);
            if (path == null) {
                ShowMessage_WithEModelVisibility("Message", "Dimension can ont be empty");
                //MessageBox.Show("Path is invalid");
                return;
            }
            
            //AppTemplateData dat = path.GetSerialisedDataByFilePath<AppTemplateData>();

            //foreach(var data in dat.TemplateInputDataList)
            //{
            //    theTemplate.ProjectInputs.Add(new InputData(data.InputName,
            //        data.InputDefaultValue,
            //        false,
            //        data.MinimumValue,
            //        data.MaximumValue));
            //    Console.WriteLine("Input added");
            //}
            path.LoadSerialisedDataToTemplate(ref theTemplate);

            // need to load the description as well
        }

        private async void OnProjectGenerateDXF(object sender, RoutedEventArgs e)
        {
            foreach (var input in theTemplate.ProjectInputs)
            {
                if (input.IsNotConfirmed)
                {
                    ShowMessage_WithEModelVisibility("Message", "Dimension can ont be empty");
                    //MessageBox.Show("Please confirm all the inputs");
                    return;
                }
            }
            CommonOpenFileDialog dlg = new CommonOpenFileDialog();
            dlg.Title = "Please select a path to save";
            dlg.IsFolderPicker = true;

            dlg.AddToMostRecentlyUsedList = true;
            dlg.AllowNonFileSystemItems = true;
            dlg.EnsurePathExists = true;
            dlg.Multiselect = false;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                theTemplate.SLDEModelVisibility = Visibility.Hidden;
                Task task1 = ProjectGenerateDXF(dlg.FileName);
                await task1.ContinueWith((T) => { theTemplate.SLDEModelVisibility = Visibility.Visible; });
            }
        }

        private async Task ProjectGenerateDXF(string dlgFilName)
        {
            ProgressDialogController progressDialogController = await this.ShowProgressAsync("Please wait patiently", "A Solidworks task is running.", false, new MetroDialogSettings { AnimateHide = false });
            progressDialogController.SetIndeterminate();

            bool result = await Task.Run(() =>
            {
                try
                {
                    if (SolidworksManager.TryCreatANewSolidworksModel(ref theTemplate))
                    {
                        if (SolidworksManager.TryCreatingDXFFilesFromTemporaryProject((MarkPointEnum)theTemplate.MarkPointOptionComboBox))
                        {
                            SolidworksManager.MergeDXF(dlgFilName, theTemplate.Name);
                            return true;
                        }
                    }
                    return false;
                }
                catch
                {
                    return false;
                }                
            });

            await progressDialogController.CloseAsync();

            if (result)
            {
                await this.ShowMessageAsync("Message", "Failed", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }
            else
            {
                await this.ShowMessageAsync("Message", "Successful", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }
        }

        /// <summary>
        /// here 25/8/2018 by Horace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnProjectGenerateDXFFromExistingFiles(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog dlg = new CommonOpenFileDialog();
            dlg.Title = "Please select a path to save";
            dlg.IsFolderPicker = true;

            dlg.AddToMostRecentlyUsedList = true;
            dlg.AllowNonFileSystemItems = true;
            dlg.EnsurePathExists = true;
            dlg.Multiselect = false;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {                
                theTemplate.SLDEModelVisibility = Visibility.Hidden;
                Task task1 = OnTryCreatingDXFFilesFromTemporaryProject(dlg.FileName, theTemplate.Name);
                await task1.ContinueWith((T)=> { theTemplate.SLDEModelVisibility = Visibility.Visible; });
            }
        }

        private async Task OnTryCreatingDXFFilesFromTemporaryProject(string TargetPath, string FileName)
        {            
            ProgressDialogController progressDialogController = await this.ShowProgressAsync("Please wait patiently", "A Solidworks task is running.", false, new MetroDialogSettings { AnimateHide = false });
            progressDialogController.SetIndeterminate();

            var result = await Task.Run(() =>
            {
                return SolidworksManager.TryCreatingDXFFilesFromTemporaryProject((MarkPointEnum)theTemplate.MarkPointOptionComboBox);
            });
            progressDialogController.SetMessage("Mergining DxF files");

            if (result)
            {
                result = await Task.Run(() =>
                {
                    return SolidworksManager.MergeDXF(TargetPath, FileName);
                });
            }

            await progressDialogController.CloseAsync();

            if (!result)
            {
                await this.ShowMessageAsync("Message", "Failed", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }
            else
            {
                await this.ShowMessageAsync("Message", "Successful", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }
        }

        /// <summary>
        ///  Need to insert open file dialogue here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProjectMergeDXF(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog dlgFiles = new CommonOpenFileDialog();
            dlgFiles.Title = "Please select the files";
            dlgFiles.IsFolderPicker = false;            

            dlgFiles.AddToMostRecentlyUsedList = true;
            dlgFiles.AllowNonFileSystemItems = true;
            dlgFiles.EnsurePathExists = true;
            dlgFiles.Multiselect = true;

            CommonOpenFileDialog dlgFolder = new CommonOpenFileDialog();
            dlgFolder.Title = "Please select a path to save";
            dlgFolder.IsFolderPicker = true;

            dlgFolder.AddToMostRecentlyUsedList = true;
            dlgFolder.AllowNonFileSystemItems = true;
            dlgFolder.EnsurePathExists = true;
            dlgFolder.Multiselect = false;

            if (dlgFiles.ShowDialog() == CommonFileDialogResult.Ok)
            {
                if (dlgFolder.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    theTemplate.SLDEModelVisibility = Visibility.Hidden;
                    Task task = ProjectMergeDXF(dlgFiles.FileNames.ToArray(), dlgFolder.FileName);
                    task.ContinueWith((t) => {
                        theTemplate.SLDEModelVisibility = Visibility.Visible;                        
                    });
                    //AutoCADManager.ImportDXF(dlgFiles.FileNames.ToArray(), theTemplate.Name, dlgFolder.FileName, Autodesk.AutoCAD.Interop.Common.AcSaveAsType.ac2000_dxf);
                }
            }
        }

        private async Task ProjectMergeDXF(string[] dlgFileNames,string fileName)
        {
            ProgressDialogController progressDialogController = await this.ShowProgressAsync("Please wait patiently", "A Solidworks task is running.", false, new MetroDialogSettings { AnimateHide = false });
            progressDialogController.SetIndeterminate();

            bool result = await Task.Run(() =>
            {
                try
                {
                    return AutoCADManager.ImportDXF(dlgFileNames, theTemplate.Name, fileName, Autodesk.AutoCAD.Interop.Common.AcSaveAsType.ac2000_dxf);
                }
                catch
                {
                    return false;
                }
            });

            await progressDialogController.CloseAsync();

            if (result)
            {
                await this.ShowMessageAsync("Message", "Failed", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }
            else
            {
                await this.ShowMessageAsync("Message", "Successful", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }
        }

        private void OnEditSOLIDWORKSFile(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("File path is : " + theTemplate.FilePath);
            //SolidworksManager.TryOpenCurrentSldworkTemplate(theTemplate.FilePath);

            if (File.Exists(theTemplate.FilePath))
            {
                SolidworksManager.TryOpenCurrentSldworkTemplate(theTemplate.FilePath);
            }
            else
            {
                ShowMessage_WithEModelVisibility("Message", "Dimension can ont be empty");
                //MessageBox.Show("Solidworks file do not exist");
            }
        }

        private void TestFunction(object sender,RoutedEventArgs e)
        {
            Console.WriteLine("testing function is running ");
            //Flyout1.IsOpen = !Flyout1.IsOpen;
            theTemplate.Flyout1IsOpen = !theTemplate.Flyout1IsOpen;

            theTemplate.SLDEModelVisibility = Visibility.Hidden;
            Task task = Test1(sender,e);
            task.ContinueWith((t)=> {
                theTemplate.SLDEModelVisibility = Visibility.Visible;
            } );            
        }

        private void MouseEnterTheLeftSideMenuStackPanel(object sender, MouseEventArgs e)
        {
            Console.WriteLine("tested");
            theTemplate.Flyout1IsOpen = !theTemplate.Flyout1IsOpen;
        }


        private async Task Test1(object sender, RoutedEventArgs e)
        {
            ProgressDialogController progressDialogController = await this.ShowProgressAsync("Please wait patiently", "A Solidworks task is running.", false, new MetroDialogSettings { AnimateHide = false });

            progressDialogController.SetIndeterminate();

            await Task.Delay(5000); // do solidworks task here,......
            
            progressDialogController.SetCancelable(true);
            
            await progressDialogController.CloseAsync();
            MessageDialogResult messageDialogResult = new MessageDialogResult();
            if (progressDialogController.IsCanceled)
            {
                messageDialogResult = await this.ShowMessageAsync("Message", "You stopped the process!", MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false});
            }
            else
            {
                messageDialogResult = await this.ShowMessageAsync("Message", "Successful",MessageDialogStyle.Affirmative, new MetroDialogSettings { AnimateShow = false });
            }               
        }        

        private void SetEModelVisibility(Visibility visibility)
        {
            theTemplate.SLDEModelVisibility = Visibility.Visible;
        }

        private int test11(int i)
        {            
            System.Threading.Thread.Sleep(2000);
            return i;
        }
        #endregion        


        #region Menu
        private void SetAutoCADPath(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                AppManager.programData.AutoCADExe_FilePath = openFileDialog.FileName;
            }
        }

        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            AppManager.OnQuit();
            //e.Cancel = true;
        }
        #endregion
    }
}
